/*
 * Created by JFormDesigner on Sat Jan 25 03:19:22 EET 2020
 */

package sima.we.client.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import sima.we.common.Email;

/**
 * @author Anastasios Simeonidis
 */
public class EmailUI extends JPanel {

    private JLabel senderLabel;
    private JLabel receiverLabel;
    private JLabel uuidLabel;
    private JLabel replyLabel;
    private JLabel subjectArea;
    private JLabel timestampLabel;
    private JTextArea bodyArea;

    public EmailUI() {
        fillContents();
    }

    private void fillContents() {
        JLabel label1 = new JLabel();
        JLabel label2 = new JLabel();
        JLabel label3 = new JLabel();
        JLabel label4 = new JLabel();
        JLabel label5 = new JLabel();
        JLabel label6 = new JLabel();
        JSeparator separator1 = new JSeparator();
        JSeparator separator2 = new JSeparator();
        JSeparator separator3 = new JSeparator();
        JSeparator separator4 = new JSeparator();
        JSeparator separator5 = new JSeparator();
        JSeparator separator6 = new JSeparator();
        JSeparator separator7 = new JSeparator();
        JScrollPane scrollPane1 = new JScrollPane();

        senderLabel = new JLabel();
        receiverLabel = new JLabel();
        uuidLabel = new JLabel();
        replyLabel = new JLabel();
        subjectArea = new JLabel();
        timestampLabel = new JLabel();
        bodyArea = new JTextArea();

        //======== this ========
        setLayout(new GridBagLayout());

        //---- label1 ----
        label1.setText("Sender");
        add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- senderLabel ----
        senderLabel.setText("<sender>");
        add(senderLabel, new GridBagConstraints(2, 0, 6, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 0), 0, 0));

        //---- label2 ----
        label2.setText("Receiver");
        add(label2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- receiverLabel ----
        receiverLabel.setText("<receiver>");
        add(receiverLabel, new GridBagConstraints(2, 2, 6, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 0), 0, 0));

        //---- label3 ----
        label3.setText("ID");
        add(label3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- uuidLabel ----
        uuidLabel.setText("<uuid>");
        add(uuidLabel, new GridBagConstraints(2, 4, 1, 1, 0.3, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- label6 ----
        label6.setText("Subject");
        add(label6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- separator1 ----
        separator1.setOrientation(SwingConstants.VERTICAL);
        add(separator1, new GridBagConstraints(1, 0, 1, 8, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 3, 5), 0, 0));
        add(separator4, new GridBagConstraints(0, 1, 8, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 0), 0, 0));
        add(separator5, new GridBagConstraints(0, 3, 8, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 0), 0, 0));

        //---- separator2 ----
        separator2.setOrientation(SwingConstants.VERTICAL);
        add(separator2, new GridBagConstraints(3, 3, 1, 5, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- label4 ----
        label4.setText("Reply to");
        add(label4, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- separator3 ----
        separator3.setOrientation(SwingConstants.VERTICAL);
        add(separator3, new GridBagConstraints(5, 3, 1, 5, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- replyLabel ----
        replyLabel.setText("<replyUUID>");
        add(replyLabel, new GridBagConstraints(6, 4, 2, 1, 0.3, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 0), 0, 0));
        add(separator6, new GridBagConstraints(0, 5, 8, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 0), 0, 0));

        //---- subjectArea ----
        subjectArea.setText("<subject>");
        add(subjectArea, new GridBagConstraints(2, 6, 1, 1, 0.5, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- label5 ----
        label5.setText("Date");
        add(label5, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- timestampLabel ----
        timestampLabel.setText("<timestamp>");
        add(timestampLabel, new GridBagConstraints(6, 6, 2, 1, 0.3, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 0), 0, 0));
        add(separator7, new GridBagConstraints(0, 7, 8, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 0), 0, 0));

        //======== scrollPane1 ========
        {

            //---- bodyArea ----
            bodyArea.setText("<body>");
            bodyArea.setEditable(false);
            scrollPane1.setViewportView(bodyArea);
        }
        add(scrollPane1, new GridBagConstraints(0, 8, 8, 1, 1.0, 1.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 0), 0, 0));
    }

    public void viewEmail(Email email) {
        senderLabel.setText(email.getSender().getUsername());
        receiverLabel.setText(email.getReceiver().getUsername());
        uuidLabel.setText(email.getUUID().toString());
        replyLabel.setText(email.getReply().toString());
        subjectArea.setText(email.getSubject());
        timestampLabel.setText(
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
                new Date(email.getTimestamp())
            )
        );
        bodyArea.setText(email.getBody());
    }
}
