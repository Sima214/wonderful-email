/*
 * Created by JFormDesigner on Sat Jan 25 20:19:21 EET 2020
 */

package sima.we.client.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import sima.we.common.Email;
import sima.we.common.Email.UUID;
import sima.we.common.User;

/**
 * @author Anastasios Simeonidis
 */
public class EmailEditUI extends JPanel {

    private final User receiver;
    private UUID replyTo;

    private JTextField subjectField;
    private JEditorPane bodyEditor;

    public EmailEditUI(User selectedUser, Email replyTo) {
        this.receiver = selectedUser;
        if (replyTo != null) {
            this.replyTo = replyTo.getUUID();
        }
        fillContents();
    }

    private void fillContents() {
        JLabel label1 = new JLabel();
        JLabel label2 = new JLabel();
        JLabel receiverLabel = new JLabel();
        JLabel label4 = new JLabel();
        JLabel replyToLabel = new JLabel();
        JSeparator separator1 = new JSeparator();
        JSeparator separator2 = new JSeparator();
        JSeparator separator3 = new JSeparator();
        JSeparator separator4 = new JSeparator();
        JScrollPane scrollPane1 = new JScrollPane();

        subjectField = new JTextField();
        bodyEditor = new JEditorPane();

        //======== this ========
        setLayout(new GridBagLayout());

        //---- label4 ----
        label4.setText("Reply To");
        add(label4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- separator1 ----
        separator1.setOrientation(SwingConstants.VERTICAL);
        add(separator1, new GridBagConstraints(1, 0, 1, 6, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 3, 5), 0, 0));
        replyToLabel.setText(replyTo != null ? replyTo.toString() : "Not replying");
        add(replyToLabel, new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 0), 0, 0));
        add(separator2, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- label2 ----
        label2.setText("Sending To");
        add(label2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));
        receiverLabel.setText(receiver.getUsername());
        add(receiverLabel, new GridBagConstraints(2, 2, 1, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));
        add(separator4, new GridBagConstraints(0, 3, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 5), 0, 0));

        //---- label1 ----
        label1.setText("Subject");
        add(label1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));
        add(subjectField, new GridBagConstraints(2, 4, 1, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 3, 5), 0, 0));
        add(separator3, new GridBagConstraints(0, 5, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 3, 5), 0, 0));

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(bodyEditor);
        }
        add(scrollPane1, new GridBagConstraints(0, 6, 3, 1, 1.0, 1.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
    }

    public Email compose() {
        return Email.Factory.createDraft(receiver, replyTo, subjectField.getText(), bodyEditor.getText());
    }

}
