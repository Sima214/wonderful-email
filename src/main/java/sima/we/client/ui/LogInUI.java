/*
 * Created by JFormDesigner on Sat Jan 25 01:33:49 EET 2020
 */

package sima.we.client.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @author Anastasios Simeonidis
 */
public class LogInUI extends JPanel {

    private JTextField usernameField;
    private JTextField passwordField;

    public LogInUI() {
        fillContents();
    }

    private void fillContents() {
        JLabel usernameLabel = new JLabel();
        JLabel passwordLabel = new JLabel();
        usernameField = new JTextField();
        passwordField = new JPasswordField();

        setLayout(new GridBagLayout());

        usernameLabel.setText("Username");
        add(usernameLabel,
            new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 3, 5), 0, 0));
        add(new JSeparator(SwingConstants.VERTICAL),
            new GridBagConstraints(1, 0, 1, 3, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 5, 0));
        add(usernameField,
            new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 3, 0), 0, 0));

        passwordLabel.setText("Password");
        add(new JSeparator(),
            new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 3));
        add(passwordLabel,
            new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
        add(passwordField,
            new GridBagConstraints(2, 2, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
    }

    public String getUsername() {
        return usernameField.getText();
    }

    public String getPassword() {
        return passwordField.getText();
    }
}
