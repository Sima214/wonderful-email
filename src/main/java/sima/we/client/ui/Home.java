/*
 * Created by JFormDesigner on Sat Jan 25 03:19:00 EET 2020
 */

package sima.we.client.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import sima.we.client.ClientProtocol;
import sima.we.common.Account.Inbox;
import sima.we.common.Email;
import sima.we.common.LogUtils;
import sima.we.common.User;

/**
 * @author Anastasios Simeonidis
 */
public class Home extends JPanel {

    private final ClientProtocol com;
    private final JFrame window;

    private DefaultMutableTreeNode inboxRoot;
    private DefaultListModel<User> userModel;

    private JTree inboxTree;
    private JList<User> userList;
    private JButton emailNew;
    private JButton emailDelete;
    private JButton inboxRefresh;
    private JButton userRefresh;
    private JButton logOut;
    private EmailUI emailView;

    public Home(JFrame window, ClientProtocol com) {
        this.com = com;
        this.window = window;
        fillContents();
    }


    private void fillContents() {
        JScrollPane inboxScroll = new JScrollPane();
        JScrollPane userScroll = new JScrollPane();
        JPanel buttons = new JPanel();

        inboxRoot = new DefaultMutableTreeNode("hidden");
        inboxTree = new JTree(inboxRoot);

        userModel = new DefaultListModel<>();
        userList = new JList<>(userModel);

        emailNew = new JButton();
        emailDelete = new JButton();
        inboxRefresh = new JButton();
        userRefresh = new JButton();
        logOut = new JButton();
        emailView = new EmailUI();

        //======== this ========
        setLayout(new BorderLayout());

        //---- inboxTree ----
        inboxTree.setCellRenderer(new InboxRenderer());
        inboxTree.setMinimumSize(new Dimension(120, 120));
        inboxTree.setBorder(
            BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Inbox"));
        inboxTree.setRootVisible(false);
        inboxTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        inboxScroll.setViewportView(inboxTree);
        add(inboxScroll, BorderLayout.WEST);

        //---- userList ----
        userList.setCellRenderer(new UsersRendered());
        userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        userList.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Users"));
        userScroll.setViewportView(userList);
        add(userScroll, BorderLayout.EAST);

        //======== buttons ========
        buttons.setLayout(new FlowLayout(FlowLayout.TRAILING, 9, 6));
        //---- emailNew ----
        emailNew.setText("New Email");
        buttons.add(emailNew);
        //---- emailDelete ----
        emailDelete.setText("Delete Email");
        buttons.add(emailDelete);
        //---- inboxRefresh ----
        inboxRefresh.setText("Refresh Inbox");
        buttons.add(inboxRefresh);
        //---- userRefresh ----
        userRefresh.setText("Refresh Users");
        buttons.add(userRefresh);
        //---- logOut ----
        logOut.setText("Log Out");
        buttons.add(logOut);
        add(buttons, BorderLayout.SOUTH);
        add(emailView, BorderLayout.CENTER);

        // Fill Inbox & Events.
        updateInbox();
        inboxTree.addTreeSelectionListener(e -> {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                inboxTree.getLastSelectedPathComponent();
            if (node != null && !node.getAllowsChildren()) {
                Email email = (Email) node.getUserObject();
                Email fullEmail = com.readEmail(email.getUUID());
                if (fullEmail != null) {
                    emailView.viewEmail(fullEmail);
                } else {
                    JOptionPane.showConfirmDialog(window,
                        "Couldn't read email!", "Error",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        // Fill User List & Events.
        updateUserList();

        // Button events.
        emailNew.addActionListener(e -> {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                inboxTree.getLastSelectedPathComponent();
            final Email replyTo = (node != null && !node.getAllowsChildren()) ?
                (Email) node.getUserObject() :
                null;
            final User selectedUser = userList.getSelectedValue();
            if (selectedUser != null) {
                SwingUtilities.invokeLater(() -> {
                    EmailEditUI ui = new EmailEditUI(selectedUser, replyTo);
                    String[] options = new String[]{"Send", "Cancel"};
                    int selection = JOptionPane.showOptionDialog(
                        window, ui, "New Email Editor",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                        options, options[1]
                    );
                    switch (selection) {
                        case 0:
                            Email finalEmail = ui.compose();
                            if (finalEmail != null) {
                                String msg = com.sendEmail(finalEmail);
                                if (msg != null) {
                                    JOptionPane.showConfirmDialog(window, msg, "New Email Status",
                                        JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE);
                                }
                                updateInbox();
                            }
                            break;
                        case -1:
                        case 1:
                            break;
                        default:
                            throw new RuntimeException("Invalid selection: " + selection);
                    }
                });
            } else {
                JOptionPane.showConfirmDialog(
                    window, "Please select the User to send to.",
                    "New Email Editor", JOptionPane.DEFAULT_OPTION
                );
            }
        });
        emailDelete.addActionListener(e -> {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                inboxTree.getLastSelectedPathComponent();
            if (node != null && !node.getAllowsChildren()) {
                Email email = (Email) node.getUserObject();
                SwingUtilities.invokeLater(() -> {
                    String msg = com.deleteEmail(email.getUUID());
                    JOptionPane.showConfirmDialog(null, msg,
                        "Email deletion status", JOptionPane.DEFAULT_OPTION);
                    updateInbox();
                });
            }
        });
        inboxRefresh.addActionListener(e -> updateInbox());
        userRefresh.addActionListener(e -> updateUserList());
        logOut.addActionListener(e -> {
            com.logOut();
            window.dispose();
        });
    }

    private void updateInbox() {
        LogUtils.debug("Updating inbox!");
        inboxRoot.removeAllChildren();
        Inbox inbox = com.retrieveInbox();
        if (inbox == null) {
            LogUtils.warn("Didn't get inbox!");
            window.dispose();
            return;
        }
        Map<String, List<Email>> inboxMap = inbox.getMap();
        for (Map.Entry<String, List<Email>> kv : inboxMap.entrySet()) {
            DefaultMutableTreeNode category = new DefaultMutableTreeNode(kv.getKey());
            LogUtils.debug(String.format("Adding %d emails to %s", kv.getValue().size(), kv.getKey()));
            for (Email e : kv.getValue()) {
                category.add(new DefaultMutableTreeNode(e, false));
            }
            inboxRoot.add(category);
        }
        // Required to make new entries visible.
        DefaultTreeModel model = (DefaultTreeModel) inboxTree.getModel();
        model.reload();
        for (int i = 0; i < inboxTree.getRowCount(); i++) {
            inboxTree.expandRow(i);
        }
    }

    private void updateUserList() {
        LogUtils.debug("Updating users!");
        userModel.removeAllElements();
        User[] users = com.retrieveUsers();
        if (users == null) {
            LogUtils.warn("Didn't get users!");
            window.dispose();
            return;
        }
        userModel.addAll(Arrays.asList(users));
    }

    private static class InboxRenderer extends DefaultTreeCellRenderer {

        @Override
        public Component getTreeCellRendererComponent(
            JTree tree, Object value, boolean sel,
            boolean expanded, boolean leaf, int row, boolean hasFocus
        ) {
            super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            if (leaf && !node.getAllowsChildren()) {
                Email e = (Email) node.getUserObject();
                this.setText(String.format(
                    "<html>[%d] From: %s at %s<br>[%s] Subject: %s</html>",
                    e.getUUID().getId(),
                    e.getSender().getUsername(),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(e.getTimestamp())),
                    e.isNew() ? "Unread" : "Read",
                    e.getSubject()
                ));
            }
            return this;
        }
    }

    private static class UsersRendered extends DefaultListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            User u = (User) value;
            // setText(u.getUsername());
            // setForeground(Util.mixColors(getForeground(), u.isOnline() ? Color.GREEN : Color.RED, 1.0f));
            setText(String.format("%s (%s)", u.getUsername(), u.isOnline() ? "Online" : "Offline"));
            return this;
        }
    }
}
