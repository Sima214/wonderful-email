/*
 * Created by JFormDesigner on Fri Jan 24 15:28:32 EET 2020
 */

package sima.we.client.ui;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import sima.we.common.LogUtils;

/**
 * Auto generated dialog and then fixed up manually. This lets the user select server hostname, port and
 * certificate file.
 *
 * @author Anastasios Simeonidis
 */
public class Connect extends JPanel {

    private JTextField ipField;
    private JSpinner portSpinner;
    private JButton certificateChooser;
    private JTextField certificatePassword;

    private String address;
    private int port;
    private Path cert;
    private String certPass;

    /**
     * True if the User pressed the OK button.
     */
    private boolean accepted;

    public Connect(String defaultAddress, int defaultPort, Path defaultCertPath, String defaultCertPass) {
        super();
        this.address = defaultAddress;
        this.port = defaultPort;
        this.cert = defaultCertPath;
        this.certPass = defaultCertPass;
        fillContents();
    }

    private void fillContents() {
        // Mostly generated using JFormDesigner Evaluation.
        JLabel rootLabel = new JLabel();
        JLabel ipLabel = new JLabel();
        JLabel portLabel = new JLabel();
        JLabel certificateLabel = new JLabel();
        JLabel certificatePasswordLabel = new JLabel();
        ipField = new JTextField();
        portSpinner = new JSpinner(new SpinnerNumberModel(port, 1, 65535, 1));
        certificateChooser = new JButton();
        certificatePassword = new JTextField();

        setLayout(new GridBagLayout());

        //---- rootLabel ----
        rootLabel.setText("Server Select");
        Font defaultFont = rootLabel.getFont();
        if (defaultFont != null) {
            rootLabel.setFont(defaultFont.deriveFont(Font.BOLD, 18));
        } else {
            rootLabel.setFont(new Font("serif", Font.BOLD, 18));
        }
        add(rootLabel,
            new GridBagConstraints(0, 0, 3, 1, 0.25, 1.8,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 3, 3, 0), 0, 0));

        // Upper Separator.
        add(new JSeparator(),
            new GridBagConstraints(0, 1, 3, 1, 0.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 0));

        //---- ipLabel ----
        ipLabel.setText("Server Address");
        add(ipLabel,
            new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE,
                new Insets(0, 0, 3, 5), 0, 0));

        //---- separator3 ----
        add(new JSeparator(SwingConstants.VERTICAL),
            new GridBagConstraints(1, 1, 1, 8, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 3, 5), 5, 0));
        ipField.setText(address);
        add(ipField,
            new GridBagConstraints(2, 2, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 0));
        add(new JSeparator(),
            new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 0));

        //---- portLabel ----
        portLabel.setText("Server Port");
        add(portLabel,
            new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE,
                new Insets(0, 0, 3, 5), 0, 0));
        add(portSpinner,
            new GridBagConstraints(2, 4, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 0));
        add(new JSeparator(),
            new GridBagConstraints(0, 5, 3, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 3));

        //---- certificateLabel ----
        certificateLabel.setText("Certificate");
        add(certificateLabel,
            new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE,
                new Insets(0, 0, 3, 5), 0, 0));

        //---- certificateChooser ----
        certificateChooser.setText("(File Chooser) " + cert.toString());
        add(certificateChooser,
            new GridBagConstraints(2, 6, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 0));
        //add(new JSeparator(),
        //    new GridBagConstraints(0, 6, 3, 1, 1.0, 1.0,
        //        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        //        new Insets(0, 0, 3, 0), 0, 3));
        // Certificate password.
        certificatePasswordLabel.setText("Certificate Password");
        add(certificatePasswordLabel,
            new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE,
                new Insets(0, 0, 3, 5), 0, 0));
        certificatePassword.setText(certPass);
        add(certificatePassword,
            new GridBagConstraints(2, 7, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 3, 0), 0, 0));
        // Bottom filler.
        add(new JPanel(null),
            new GridBagConstraints(0, 8, 3, 1, 0.0, 1.8,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));

        // Certificate File Chooser handlers.
        certificateChooser.addActionListener(e -> {
            final JFileChooser fc = new JFileChooser(cert.getParent().toFile());
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                Path newCert = FileSystems.getDefault().getPath(fc.getSelectedFile().getAbsolutePath());
                cert = newCert.toAbsolutePath().normalize();
                LogUtils.debug("New certificate: " + cert.toString());
                certificateChooser.setText("(File Chooser) " + cert.toString());
            }
        });
    }

    public String getIPString() {
        address = ipField.getText();
        return address;
    }

    public int getPort() {
        port = ((SpinnerNumberModel) portSpinner.getModel()).getNumber().intValue();
        return port;
    }

    public String getCertificate() {
        return cert.toString();
    }

    public String getCertificatePassword() {
        certPass = certificatePassword.getText();
        return certPass;
    }
}
