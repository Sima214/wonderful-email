package sima.we.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import sima.we.client.ui.Connect;
import sima.we.client.ui.Home;
import sima.we.client.ui.LogInUI;
import sima.we.common.ConfigProvider;
import sima.we.common.LogUtils;

/**
 * Client lifecycle scripting.
 *
 * @author Anastasios Symeonidis
 */
public class Entry {

    /**
     * Optional lifecycle step. Here the user is presented with a dialog to choose a server.
     */
    private static ClientProtocol establishConnection() {
        ClientProtocol ret = new ClientProtocol();
        final ConfigProvider cp = ConfigProvider.instance;
        // Try to connect using only command line arguments.
        String msg = ret.connectWith(cp.getIPAddress(), cp.getPort());
        if (msg != null) {
            Util.runInUIThread(() -> JOptionPane.showConfirmDialog(
                null, msg, "Handshake report", JOptionPane.DEFAULT_OPTION
            ));
        }
        // Show dialog until we've made a successful connection.
        while (!ret.isConnected() && !ret.isClosed()) {
            Util.runInUIThread(() -> {
                LogUtils.info("Showing server picker dialog.");
                // Show dialog.
                Connect ui = new Connect(
                    cp.getIPAddress() != null ? cp.getIPAddress().getHostAddress() : "",
                    cp.getPort(),
                    FileSystems.getDefault().getPath(cp.getCertificate()).toAbsolutePath().normalize(),
                    cp.getCertificatePassword());
                int dialog_option = JOptionPane.showConfirmDialog(null, ui,
                    "Server Connect Information", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.PLAIN_MESSAGE);
                if (dialog_option == 0) {
                    // Process user input.
                    try {
                        InetAddress serverIP = InetAddress.getByName(ui.getIPString());
                        int serverPort = ui.getPort();
                        Util.setCertificate(ui.getCertificate(), ui.getCertificatePassword());
                        String msg2 = ret.connectWith(serverIP, serverPort);
                        if (msg2 != null) {
                            JOptionPane.showConfirmDialog(null, msg2,
                                "Handshake report", JOptionPane.DEFAULT_OPTION);
                        }
                    } catch (UnknownHostException e) {
                        LogUtils.getLogger().warn("Cannot resolve provided IP address.", e);
                    }
                } else {
                    // User has selected to exit.
                    ret.close();
                }
            });
        }
        return ret;
    }

    private static boolean showLogInValidInput(String str) {
        return str != null && str.length() != 0;
    }

    private static boolean showLogInValidInput(String username, String password) {
        return showLogInValidInput(username) && showLogInValidInput(password);
    }

    private static void showLogIn(ClientProtocol comms) {
        Util.runInUIThread(() -> {
            LogInUI ui = new LogInUI();
            String[] logInButtons = new String[]{"LogIn", "Register", "Exit"};
            int selection = JOptionPane.showOptionDialog(
                null, ui, "LogIn/Register", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, logInButtons, logInButtons[2]
            );
            String username = ui.getUsername();
            String password = ui.getPassword();
            switch (selection) {
                case 0:
                    // LogIn
                    if (showLogInValidInput(username, password)) {
                        String msg = comms.logIn(username, password);
                        if (msg != null) {
                            JOptionPane.showConfirmDialog(
                                null, msg, "LogIn report", JOptionPane.DEFAULT_OPTION
                            );
                        }
                    }
                    break;
                case 1:
                    // Register
                    if (showLogInValidInput(username, password)) {
                        String msg = comms.register(username, password);
                        if (msg != null) {
                            JOptionPane.showConfirmDialog(
                                null, msg, "Register report", JOptionPane.DEFAULT_OPTION
                            );
                        }
                    }
                    break;
                case -1:
                    // Esc / Window close
                case 2:
                    // Cancel
                    comms.close();
                    break;
                default:
                    throw new RuntimeException(selection + "out of bounds!");
            }
        });
    }

    private static void showHomeScreen(ClientProtocol comms) {
        final Lock mainLock = new ReentrantLock();
        final Condition mainCond = mainLock.newCondition();
        Util.runInUIThread(() -> {
            JFrame homeWin = new JFrame("Wonderful Email");
            Home ui = new Home(homeWin, comms);
            homeWin.setContentPane(ui);
            homeWin.pack();
            homeWin.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            homeWin.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    LogUtils.debug("Home window closed.");
                    if (comms.isLoggedIn()) {
                        // Still logged in, means user is closing the app.
                        comms.close();
                    }
                    // Wake up main thread.
                    mainLock.lock();
                    mainCond.signalAll();
                    mainLock.unlock();
                }
            });
            homeWin.setVisible(true);
        });
        // Wait until Home frame gets disposed.
        mainLock.lock();
        mainCond.awaitUninterruptibly();
        mainLock.unlock();
    }

    public static void main(String[] args) {
        ConfigProvider.instance.handleArgs(args, false);
        LogUtils.info("Starting up client!");
        Util.runInUIThread(Util::setSwingTheme);
        ClientProtocol comms = establishConnection();
        // Run main logIn loop.
        while (comms.isConnected()) {
            showLogIn(comms);
            while (comms.isLoggedIn()) {
                // Run home screen.
                showHomeScreen(comms);
            }
        }
    }

}
