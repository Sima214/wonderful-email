package sima.we.client;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import sima.we.common.LogUtils;

/**
 * Swing common code and other misc code.
 *
 * @author Anastasios Symeonidis
 */
public class Util {

    /**
     * Sets swing theme.
     */
    public static void setSwingTheme() {
        // Set look and feel.
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            LogUtils.info("Successfully applied native theme.");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            // Just print the error message and hope that the gui is not horrible broken.
            LogUtils.getLogger().warn("Things may look a bit weird...", e);
        }
    }

    public static boolean runInUIThread(Runnable obj) {
        try {
            SwingUtilities.invokeAndWait(obj);
            return true;
        } catch (InterruptedException | InvocationTargetException e) {
            LogUtils.getLogger().error("An UI error occurred!", e);
            return false;
        }
    }

    public static void setCertificate(String path, String pass) {
        // Server
        System.setProperty("javax.net.ssl.keyStore", path);
        System.setProperty("javax.net.ssl.keyStorePassword", pass);
        // Client
        System.setProperty("javax.net.ssl.trustStore", path);
        System.setProperty("javax.net.ssl.trustStorePassword", pass);
    }

    public static Color mixColors(Color x, Color y, float ratio) {
        int r = (int) ((x.getRed() * (1.0 - ratio)) / 255 + (y.getRed() * ratio) / 255);
        int g = (int) ((x.getGreen() * (1.0 - ratio)) / 255 + (y.getGreen() * ratio) / 255);
        int b = (int) ((x.getBlue() * (1.0 - ratio)) / 255 + (y.getBlue() * ratio) / 255);
        return new Color(r, g, b, 255);
    }

}
