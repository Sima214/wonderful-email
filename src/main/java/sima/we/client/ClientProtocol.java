package sima.we.client;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import sima.we.common.Account.Inbox;
import sima.we.common.Email;
import sima.we.common.Email.UUID;
import sima.we.common.LogUtils;
import sima.we.common.User;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.PacketHeader.Compression;
import sima.we.common.protocol.payloads.DeleteEmail;
import sima.we.common.protocol.payloads.Handshake;
import sima.we.common.protocol.payloads.Handshake.Response;
import sima.we.common.protocol.payloads.ListUsers;
import sima.we.common.protocol.payloads.LogIn;
import sima.we.common.protocol.payloads.LogOut;
import sima.we.common.protocol.payloads.NewEmail;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.ReadEmail;
import sima.we.common.protocol.payloads.Register;
import sima.we.common.protocol.payloads.ShowEmails;

/**
 * TODO
 *
 * @author Anastasios Symeonidis
 */
public class ClientProtocol implements Closeable {

    /**
     * State variables.
     */
    private boolean closed;
    private boolean connected;
    private boolean loggedIn;

    /**
     * Protocol Options.
     */
    private InetAddress serverIP;
    private int serverPort;

    /**
     * Protocol state.
     */
    Compression compression;

    /**
     * Network resources.
     */
    private SSLSocket socket;
    private OutputStream socketOutput;
    private InputStream socketInput;

    public ClientProtocol() {
        // Initial state.
        connected = false;
        closed = false;
        loggedIn = false;
        compression = Compression.NONE;
    }

    /**
     * Tries to initiate a connection with server.
     */
    public String connectWith(InetAddress ip, int port) {
        if (connected || ip == null) {
            return null;
        }
        serverIP = ip;
        serverPort = port;
        // Try to create a TCP connection.
        LogUtils.info("Connecting to " + ip.getHostAddress() + ":" + port);
        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        try {
            socket = (SSLSocket) factory.createSocket(ip, port);
            socket.startHandshake();
            socketInput = socket.getInputStream();
            socketOutput = socket.getOutputStream();
        } catch (IOException e) {
            socket = null;
            LogUtils.getLogger().error("Couldn't create connection!", e);
            return null;
        }
        // Send Handshake request.
        LogUtils.debug("Sending handshake request...");
        PacketPayloadRequest request = new Handshake.Request();
        try {
            Packet.send(socketOutput, request, compression);
        } catch (IOException e) {
            LogUtils.getLogger().error("Couldn't handshake with server!", e);
            try {
                socket.close();
                socket = null;
            } catch (IOException ex) {
                LogUtils.getLogger().fatal("Couldn't cleanup socket!", ex);
            }
            return null;
        }
        // Get Handshake response.
        try {
            Packet packet = Packet.read(socketInput, false);
            Handshake.Response response = (Response) packet.payload;
            if (response.isAccepted()) {
                // Connection is good.
                compression = response.getCompression();
                connected = true;
            } else {
                // Error.
                LogUtils.error("Server did not accept us.");
                if (response.getMessage() != null) {
                    LogUtils.error("Reason: " + response.getMessage());
                }
                close();
            }
            return response.getMessage();
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O getting handshake response!", e);
            return null;
        }
    }

    public String logIn(String username, String password) {
        LogIn.Request request = new LogIn.Request(username, password);
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send logIn request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending logIn request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                LogIn.Response response = (LogIn.Response) packet.payload;
                if (response.isAccepted()) {
                    loggedIn = true;
                } else {
                    LogUtils.warn("LogIn failed!");
                }
                return response.getMessage();
            } else {
                LogUtils.error("Couldn't receive logIn response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving logIn response!", e);
            close();
            return null;
        }
    }

    public String register(String username, String password) {
        Register.Request request = new Register.Request(username, password);
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send register request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending register request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                Register.Response response = (Register.Response) packet.payload;
                if (response.isAccepted()) {
                    loggedIn = true;
                } else {
                    LogUtils.warn("Register failed!");
                }
                return response.getMessage();
            } else {
                LogUtils.error("Couldn't receive register response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving register response!", e);
            close();
            return null;
        }
    }

    public Inbox retrieveInbox() {
        ShowEmails.Request request = new ShowEmails.Request();
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send showEmails request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending showEmails request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                ShowEmails.Response response = (ShowEmails.Response) packet.payload;
                return response.getInbox();
            } else {
                LogUtils.error("Couldn't receive showEmails response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving showEmails response!", e);
            close();
            return null;
        }
    }

    public User[] retrieveUsers() {
        ListUsers.Request request = new ListUsers.Request();
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send listUsers request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending listUsers request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                ListUsers.Response response = (ListUsers.Response) packet.payload;
                return response.getUsers();
            } else {
                LogUtils.error("Couldn't receive listUsers response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving listUsers response!", e);
            close();
            return null;
        }
    }

    public Email readEmail(UUID uuid) {
        ReadEmail.Request request = new ReadEmail.Request(uuid);
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send readEmail request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending readEmail request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                ReadEmail.Response response = (ReadEmail.Response) packet.payload;
                return response.getEmail();
            } else {
                LogUtils.error("Couldn't receive readEmail response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving readEmail response!", e);
            close();
            return null;
        }
    }

    public String sendEmail(Email finalEmail) {
        NewEmail.Request request = new NewEmail.Request(finalEmail);
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send newEmail request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending newEmail request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                NewEmail.Response response = (NewEmail.Response) packet.payload;
                if (response.isSent()) {
                    if (response.getMessage() != null) {
                        return String.format("Successfully sent with ID: %s.\nServer Response: %s",
                            response.getUuid(), response.getMessage());
                    } else {
                        return String.format("Successfully sent with ID: %s.", response.getUuid());
                    }
                } else {
                    return response.getMessage();
                }
            } else {
                LogUtils.error("Couldn't receive newEmail response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving newEmail response!", e);
            close();
            return null;
        }
    }

    public String deleteEmail(UUID uuid) {
        DeleteEmail.Request request = new DeleteEmail.Request(uuid);
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send deleteEmail request!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending deleteEmail request!", e);
            close();
            return null;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                DeleteEmail.Response response = (DeleteEmail.Response) packet.payload;
                if (response.isDeleted()) {
                    return "Success";
                } else {
                    return "Failed";
                }
            } else {
                LogUtils.error("Couldn't receive deleteEmail response!");
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving deleteEmail response!", e);
            close();
            return null;
        }
    }

    public void logOut() {
        LogOut.Request request = new LogOut.Request();
        try {
            if (!Packet.send(socketOutput, request, compression)) {
                LogUtils.error("Couldn't send logOut request!");
                return;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error sending logOut request!", e);
            close();
            return;
        }

        try {
            Packet packet = Packet.read(socketInput, false);
            if (packet != null) {
                LogOut.Response response = (LogOut.Response) packet.payload;
                loggedIn = false;
                return;
            } else {
                LogUtils.error("Couldn't receive logOut response!");
                return;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error receiving logOut response!", e);
            close();
            return;
        }
    }

    /**
     * Cleans up the object. After this is called, this object cannot be used anymore.
     */
    @Override
    public void close() {
        if (connected) {
            try {
                socket.close();
                socket = null;
            } catch (IOException e) {
                LogUtils.getLogger().error("Couldn't close socket!", e);
            }
            loggedIn = false;
            connected = false;
        }
        closed = true;
    }

    /**
     * TCP connection with server is active.
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * This object cannot be used anymore.
     */
    public boolean isClosed() {
        return closed;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }
}
