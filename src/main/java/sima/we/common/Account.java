package sima.we.common;

import com.devsmart.ubjson.UBArray;
import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import sima.we.common.Email.Factory;
import sima.we.common.Email.UUID;

/**
 * An authenticated {@link User}.
 *
 * @author Anastasios Symeonidis
 */
public class Account extends User {

    private final Inbox inbox;

    public Account(String username, Inbox inbox) {
        super(username, true);
        this.inbox = inbox;
    }

    public Account(User user, Inbox inbox) {
        super(user.username, true);
        this.inbox = inbox;
    }

    public Inbox getInbox() {
        return inbox;
    }

    /**
     * @return An unmodifiable copy of this account without any of the personal information.
     */
    public User asUser() {
        return new User(username, true);
    }

    public static class Inbox {

        private Map<String, List<Email>> inbox;

        /**
         * @param data map to use as inbox;
         */
        public Inbox(TreeMap<String, List<Email>> data) {
            inbox = data;
        }

        /**
         * Creates an inbox with default directories.
         */
        public Inbox() {
            inbox = new TreeMap<>();
            inbox.put("Received", new ArrayList<>());
            inbox.put("Sent", new ArrayList<>());
        }

        public Email find(UUID uuid) {
            for (List<Email> v : inbox.values()) {
                for (Email e : v) {
                    if (e.getUUID().equals(uuid)) {
                        return e;
                    }
                }
            }
            return null;
        }

        public boolean delete(UUID uuid) {
            for (List<Email> l : inbox.values()) {
                Iterator<Email> iter = l.iterator();
                while (iter.hasNext()) {
                    Email e = iter.next();
                    if (e.getUUID().equals(uuid)) {
                        iter.remove();
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Recreates emails by calling the provided function.
         */
        public void reload(Function<Email, Email> provider) {
            for (List<Email> emailList : inbox.values()) {
                for (int i = 0; i < emailList.size(); i++) {
                    emailList.set(i, provider.apply(emailList.get(i)));
                }
            }
        }

        public void emailReceived(Email email) {
            inbox.get("Received").add(email);
        }

        public void emailSent(Email email) {
            inbox.get("Sent").add(email);
        }

        public static Inbox getFromPartialBinaryObject(UBValue inbox) {
            if (inbox != null && inbox.isArray()) {
                // Array of categories.
                UBArray inboxArray = inbox.asArray();
                TreeMap<String, List<Email>> categories = new TreeMap<>();
                for (int i = 0; i < inboxArray.size(); i++) {
                    if (inboxArray.get(i).isObject()) {
                        UBObject inboxObject = inboxArray.get(i).asObject();
                        // Array of partial emails.
                        UBValue listing = inboxObject.get("listing");
                        ArrayList<Email> partialEmails = new ArrayList<>();
                        if (listing != null && listing.isArray()) {
                            UBArray listingArray = listing.asArray();
                            for (int j = 0; j < listingArray.size(); j++) {
                                UBValue partialEmailRoot = listingArray.get(j);
                                Email partial = Factory.getPartial(partialEmailRoot);
                                if (partial != null) {
                                    partialEmails.add(partial);
                                } else {
                                    LogUtils.warn("Received invalid partial email on listing!");
                                }
                            }
                        } else {
                            return null;
                        }
                        UBValue displayName = inboxObject.get("display_name");
                        if (displayName != null && displayName.isString()) {
                            // Successfully created new category.
                            String name = displayName.asString();
                            categories.put(name, partialEmails);
                        } else {
                            LogUtils.warn("Inbox category had no name!");
                        }
                    } else {
                        return null;
                    }
                }
                return new Inbox(categories);
            }
            return null;
        }

        public UBValue getAsPartialBinaryObject() {
            UBObject[] categories = new UBObject[inbox.size()];
            int categoryIndex = 0;
            for (Map.Entry<String, List<Email>> kv : inbox.entrySet()) {
                // First convert all emails.
                List<Email> emailListing = kv.getValue();
                UBValue[] objectListing = new UBObject[emailListing.size()];
                for (int i = 0; i < emailListing.size(); i++) {
                    objectListing[i] = emailListing.get(i).getPartialBinaryObject();
                }
                // Create category.
                UBObject category = UBValueFactory.createObject();
                UBArray arrayListing = UBValueFactory.createArray(objectListing);
                category.put("display_name", UBValueFactory.createString(kv.getKey()));
                category.put("listing", arrayListing);
                categories[categoryIndex] = category;
                categoryIndex++;
            }
            return UBValueFactory.createArray(categories);
        }

        public UBValue getAsBinaryObject(int emailOptions) {
            UBObject[] categories = new UBObject[inbox.size()];
            int categoryIndex = 0;
            for (Map.Entry<String, List<Email>> kv : inbox.entrySet()) {
                // First convert all emails.
                List<Email> emailListing = kv.getValue();
                UBValue[] objectListing = new UBObject[emailListing.size()];
                for (int i = 0; i < emailListing.size(); i++) {
                    objectListing[i] = emailListing.get(i).getAsBinaryObject(emailOptions);
                }
                // Create category.
                UBObject category = UBValueFactory.createObject();
                UBArray arrayListing = UBValueFactory.createArray(objectListing);
                category.put("display_name", UBValueFactory.createString(kv.getKey()));
                category.put("listing", arrayListing);
                categories[categoryIndex] = category;
                categoryIndex++;
            }
            return UBValueFactory.createArray(categories);
        }

        public boolean getFromBinaryObject(UBValue inbox, int emailOptions) {
            if (inbox != null && inbox.isArray()) {
                // Array of categories.
                UBArray inboxArray = inbox.asArray();
                TreeMap<String, List<Email>> categories = new TreeMap<>();
                for (int i = 0; i < inboxArray.size(); i++) {
                    if (inboxArray.get(i).isObject()) {
                        UBObject inboxObject = inboxArray.get(i).asObject();
                        // Array of partial emails.
                        UBValue listing = inboxObject.get("listing");
                        ArrayList<Email> emails = new ArrayList<>();
                        if (listing != null && listing.isArray()) {
                            UBArray listingArray = listing.asArray();
                            for (int j = 0; j < listingArray.size(); j++) {
                                UBValue emailRoot = listingArray.get(j);
                                Email email = Factory.get(emailRoot, emailOptions);
                                if (email != null) {
                                    emails.add(email);
                                } else {
                                    LogUtils.warn("Received invalid email on listing!");
                                }
                            }
                        } else {
                            return false;
                        }
                        UBValue displayName = inboxObject.get("display_name");
                        if (displayName != null && displayName.isString()) {
                            // Successfully created new category.
                            String name = displayName.asString();
                            categories.put(name, emails);
                        } else {
                            LogUtils.warn("Inbox category had no name!");
                        }
                    } else {
                        return false;
                    }
                }
                this.inbox = categories;
                return true;
            }
            return false;
        }

        /**
         * @return A deep copy, but with partial emails.
         */
        public Inbox partialCopy() {
            TreeMap<String, List<Email>> copy = new TreeMap<>();
            for (Map.Entry<String, List<Email>> kv : inbox.entrySet()) {
                List<Email> original = kv.getValue();
                ArrayList<Email> cloned = new ArrayList<>(original.size());
                for (Email e : original) {
                    cloned.add(Email.Factory.getPartial(e));
                }
                copy.put(kv.getKey(), cloned);
            }
            return new Inbox(copy);
        }

        public Map<String, List<Email>> getMap() {
            return inbox;
        }
    }
}