package sima.we.common;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import java.util.Objects;

/**
 * A generic class class representing a non-authenticated or authenticated user.
 *
 * @author Anastasios Symeonidis
 */
public class User {

    /**
     * You cannot actually send emails to this user. This is used only as a fallback.
     */
    public static final User DEFAULT_USER = new User("Anonymous");

    /**
     * Non-Null!
     */
    protected final String username;
    protected boolean online;

    /**
     * @param username Must NOT be null!
     * @param online   User current state.
     */
    public User(String username, boolean online) {
        assert username != null : "Cannot create a User without a name!";
        this.username = username;
        this.online = online;
    }

    /**
     * Creates a disconnected User.
     *
     * @param username Must NOT be null!
     */
    public User(String username) {
        this(username, false);
    }

    /**
     * @param root Decoded binary values.
     * @return The {@link User} representation of the root, or null if root does not contain a {@link
     * User} object.
     */
    public static User getFromBinaryObject(UBValue root) {
        if (root != null && root.isObject()) {
            UBObject userObj = root.asObject();
            UBValue username = userObj.get("username");
            UBValue connected = userObj.get("connected");
            if (username != null && connected != null && username.isString() && connected.isBool()) {
                return new User(username.asString(), connected.asBool());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static User getFromBinaryObjectSimple(UBValue value) {
        if (value != null && value.isString()) {
            return new User(value.asString());
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    public String getUsername() {
        return username;
    }

    public boolean isOnline() {
        return online;
    }

    public UBValue getAsBinaryObject() {
        UBObject userObj = UBValueFactory.createObject();
        userObj.put("username", UBValueFactory.createString(username));
        userObj.put("connected", UBValueFactory.createBool(online));
        return userObj;
    }

    public UBValue getAsBinaryObjectSimple() {
        return UBValueFactory.createString(username);
    }
}
