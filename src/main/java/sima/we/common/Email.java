package sima.we.common;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBValueFactoryEx;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Base64;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

/**
 * A `live` email representation.
 *
 * @author Anastasios Symeonidis
 */
public class Email {

    public static final int FIELD_UUID = 0b1 << 0;
    public static final int FIELD_IS_NEW = 0b1 << 1;
    public static final int FIELD_SENDER = 0b1 << 2;
    public static final int FIELD_RECEIVER = 0b1 << 3;
    public static final int FIELD_TIMESTAMP = 0b1 << 4;
    public static final int FIELD_REPLY = 0b1 << 5;
    public static final int FIELD_SUBJECT = 0b1 << 6;
    public static final int FIELD_BODY = 0b1 << 7;
    public static final int FIELD_ATTACHMENTS = 0b1 << 8;
    public static final int FIELD_PARTIAL = FIELD_UUID | FIELD_IS_NEW | FIELD_SENDER |
        FIELD_TIMESTAMP | FIELD_SUBJECT;
    public static final int FIELD_FULL = FIELD_UUID | FIELD_SENDER | FIELD_RECEIVER |
        FIELD_TIMESTAMP | FIELD_REPLY | FIELD_SUBJECT | FIELD_BODY | FIELD_ATTACHMENTS;
    /**
     * The contents of an email which has not been sent yet.
     */
    public static final int FIELD_NEW = ((FIELD_FULL & ~FIELD_SENDER) & ~FIELD_UUID) & ~FIELD_TIMESTAMP;

    protected final UUID uuid;
    protected boolean isNew;
    protected final User sender;
    protected final User receiver;
    protected final long timestamp;
    protected final UUID reply;
    protected final String subject;
    protected final String body;
    protected final Map<String, Attachment> attachments;

    /**
     * Internal use only. Please use factory to generate emails.
     */
    protected Email(UUID uuid, boolean isNew, User sender, User receiver, long timestamp, UUID reply,
        String subject,
        String body, Map<String, Attachment> attachments) {
        this.uuid = uuid;
        this.isNew = isNew;
        this.sender = sender;
        this.receiver = receiver;
        this.timestamp = timestamp;
        this.reply = reply;
        this.subject = subject;
        this.body = body;
        this.attachments = attachments;
    }

    /**
     * @param export The fields to export/serialize.
     * @return A ubjson object of the requested fields, or null if this {@link Email} object does not have
     * enough information(for example it is partial).
     */
    public UBValue getAsBinaryObject(int export) {
        UBObject root = UBValueFactory.createObject();
        if ((export & FIELD_UUID) != 0) {
            if (uuid == null) {
                return null;
            }
            root.put("uuid", uuid.getAsBinaryObject());
        }
        if ((export & FIELD_IS_NEW) != 0) {
            root.put("isRead", UBValueFactory.createBool(!isNew));
        }
        if ((export & FIELD_SENDER) != 0) {
            if (sender == null) {
                return null;
            }
            root.put("received_from", sender.getAsBinaryObjectSimple());
        }
        if ((export & FIELD_RECEIVER) != 0) {
            if (receiver == null) {
                return null;
            }
            root.put("sent_to", receiver.getAsBinaryObjectSimple());
        }
        if ((export & FIELD_TIMESTAMP) != 0) {
            root.put("timestamp", UBValueFactoryEx.createInt64(timestamp));
        }
        if ((export & FIELD_REPLY) != 0) {
            if (reply == null) {
                return null;
            }
            root.put("reply_uuid", reply.getAsBinaryObject());
        }
        if ((export & FIELD_SUBJECT) != 0) {
            if (subject == null) {
                return null;
            }
            root.put("subject", UBValueFactory.createString(subject));
        }
        if ((export & FIELD_BODY) != 0) {
            if (body == null) {
                return null;
            }
            root.put("body", UBValueFactory.createString(body));
        }
        if ((export & FIELD_ATTACHMENTS) != 0) {
            if (attachments == null) {
                return null;
            }
            root.put("attachments", Attachment.getAsBinaryMap(attachments));
        }
        return root;
    }

    public UBValue getPartialBinaryObject() {
        return getAsBinaryObject(FIELD_PARTIAL);
    }

    /**
     * Initializes any missing values with corrupted ones.
     *
     * @return the new normalized email
     */
    public Email normalize() {
        UUID uuid = this.uuid != null ? this.uuid : UUID.INVALID_UUID;
        boolean isNew = this.isNew;
        User sender = this.sender != null ? this.sender : User.DEFAULT_USER;
        User receiver = this.receiver != null ? this.receiver : User.DEFAULT_USER;
        long timestamp = this.timestamp;
        UUID reply = this.reply != null ? this.reply : UUID.INVALID_UUID;
        String subject = this.subject != null ? this.subject : "";
        String body = this.body != null ? this.body : "";
        Map<String, Attachment> attachments =
            this.attachments != null ? this.attachments : new TreeMap<>();
        return new Email(uuid, isNew,
            sender, receiver, timestamp, reply,
            subject, body, attachments);
    }

    public boolean isPartial() {
        return body == null;
    }

    public UUID getUUID() {
        return uuid;
    }

    public boolean isNew() {
        return isNew;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public UUID getReply() {
        return reply;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public Map<String, Attachment> getAttachments() {
        return attachments;
    }

    public void setIsNew(boolean b) {
        isNew = b;
    }

    public static class UUID {

        /**
         * Fallback value. Does not represent a valid reference to an email.
         */
        private static final UUID INVALID_UUID = new UUID(-1);

        private final long id;

        public UUID(long id) {
            this.id = id;
        }

        public UUID increment() {
            return new UUID(id + 1);
        }

        public static UUID getFromBinaryObject(UBValue value) {
            if (value != null && value.isInteger()) {
                return new UUID(value.asLong());
            }
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof UUID)) {
                return false;
            }
            UUID uuid = (UUID) o;
            return id == uuid.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        public UBValue getAsBinaryObject() {
            return UBValueFactoryEx.createInt64(id);
        }

        public boolean isValid() {
            return !this.equals(INVALID_UUID);
        }

        public long getId() {
            return id;
        }

        @Override
        public String toString() {
            return String.valueOf(getId());
        }

    }

    public static class Attachment {

        private final String mimeType;

        private final byte[] data;

        /**
         * Internal constructor. This must always be called from other constructors.
         */
        protected Attachment(String mimeType, byte[] data) {
            this.mimeType = mimeType;
            this.data = data;
        }

        /**
         * Gets the content type from the file specified by path and loads its contents into memory.
         *
         * @param path Used for {@link java.nio.file.Files probeContentType} and for an input stream.
         * @return Always returns a valid {@link Attachment} object except if an IO exception occurs.
         */
        public static Attachment createFromFile(Path path) {
            String mimeType;
            try {
                mimeType = Files.probeContentType(path);
            } catch (IOException e) {
                LogUtils.getLogger().warn("Couldn't get mime type for attachment!", e);
                return null;
            }
            byte[] data;
            try {
                data = Files.readAllBytes(path);
            } catch (IOException e) {
                LogUtils.getLogger().warn("Couldn't get file contents for attachment!", e);
                return null;
            }
            return new Attachment(mimeType, data);
        }

        public static Attachment getFromBinaryObject(UBValue root) {
            if (root != null && root.isObject()) {
                UBObject rootObj = root.asObject();
                UBValue mimeType = rootObj.get("mime_type");
                UBValue data = rootObj.get("data");
                if (mimeType != null && data != null && mimeType.isString() && data.isString()) {
                    byte[] decodedData = Base64.getDecoder().decode(data.asString());
                    return new Attachment(mimeType.asString(), decodedData);
                }
            }
            return null;
        }

        public static Map<String, Attachment> getFromBinaryMap(UBValue root) {
            if (root != null && root.isObject()) {
                UBObject rootObj = root.asObject();
                // TreeMap cause we want to retain the order.
                TreeMap<String, Attachment> ret = new TreeMap<>();
                for (Map.Entry<String, UBValue> kv : rootObj.entrySet()) {
                    String name = kv.getKey();
                    Attachment attachment = Attachment.getFromBinaryObject(kv.getValue());
                    if (attachment == null) {
                        LogUtils.warn("Dropping invalid attachment for " + name + "!");
                    } else {
                        ret.put(name, attachment);
                    }
                }
                return ret;
            }
            return null;
        }

        public UBValue getAsBinaryObject() {
            UBObject root = UBValueFactory.createObject();
            String encodedData = Base64.getEncoder().encodeToString(data);
            root.put("mime_type", UBValueFactory.createString(mimeType));
            root.put("data", UBValueFactory.createString(encodedData));
            return root;
        }

        public static UBValue getAsBinaryMap(Map<String, Attachment> map) {
            UBObject root = UBValueFactory.createObject();
            for (Entry<String, Attachment> kv : map.entrySet()) {
                root.put(kv.getKey(), kv.getValue().getAsBinaryObject());
            }
            return root;
        }

    }

    public static class Factory {

        public static Email getPartial(UUID uuid, boolean isNew, User sender, long timestamp,
            String subject) {
            return new Email(uuid, isNew, sender, null, timestamp, null, subject, null, null);
        }

        public static Email getPartial(Email email) {
            return getPartial(email.uuid, email.isNew, email.sender, email.timestamp, email.subject);
        }

        public static Email getPartial(UBValue root) {
            if (root != null && root.isObject()) {
                UBObject rootObject = root.asObject();
                UUID uuid = UUID.getFromBinaryObject(rootObject.get("uuid"));
                User sender = User.getFromBinaryObjectSimple(rootObject.get("received_from"));
                long timestamp;
                UBValue timestampValue = rootObject.get("timestamp");
                if (timestampValue != null && timestampValue.isInteger()) {
                    timestamp = timestampValue.asLong();
                } else {
                    return null;
                }
                String subject;
                UBValue subjectValue = rootObject.get("subject");
                if (subjectValue != null && subjectValue.isString()) {
                    subject = subjectValue.asString();
                } else {
                    return null;
                }
                boolean isNew = false;
                UBValue isReadValue = rootObject.get("isRead");
                if (isReadValue != null && isReadValue.isBool()) {
                    isNew = !isReadValue.asBool();
                }
                return getPartial(uuid, isNew, sender, timestamp, subject);
            } else {
                return null;
            }
        }

        /**
         * Makes a best effort to retrieve serialized information.
         *
         * @param required Bit-field of what fields are required (look at FIELD_*).
         */
        public static Email get(UBValue root, int required) {
            if (root != null && root.isObject()) {
                UBObject rootObj = root.asObject();
                UUID uuid;
                UBValue uuidValue = rootObj.get("uuid");
                uuid = UUID.getFromBinaryObject(uuidValue);
                if (uuid == null) {
                    if ((required & FIELD_UUID) != 0) {
                        LogUtils.warn("UUID was required but none was found!");
                        return null;
                    } else {
                        uuid = UUID.INVALID_UUID;
                    }
                }
                boolean isNew;
                UBValue isReadValue = rootObj.get("isRead");
                if (isReadValue != null && isReadValue.isBool()) {
                    isNew = !isReadValue.asBool();
                } else if ((required & FIELD_IS_NEW) != 0) {
                    LogUtils.warn("isNew property was required but none was found!");
                    return null;
                } else {
                    isNew = false;
                }
                User sender;
                UBValue senderValue = rootObj.get("received_from");
                sender = User.getFromBinaryObjectSimple(senderValue);
                if (sender == null && (required & FIELD_SENDER) != 0) {
                    LogUtils.warn("Sender was required but none was found!");
                    return null;
                }
                User receiver;
                UBValue receiverValue = rootObj.get("sent_to");
                receiver = User.getFromBinaryObjectSimple(receiverValue);
                if (receiver == null && (required & FIELD_RECEIVER) != 0) {
                    LogUtils.warn("Receiver was required but none was found!");
                    return null;
                }
                long timestamp;
                UBValue timestampValue = rootObj.get("timestamp");
                if (timestampValue != null && timestampValue.isInteger()) {
                    timestamp = timestampValue.asLong();
                } else if ((required & FIELD_TIMESTAMP) != 0) {
                    LogUtils.warn("Timestamp was required but none was found!");
                    return null;
                } else {
                    timestamp = 0;
                }
                UUID reply;
                UBValue replyValue = rootObj.get("reply_uuid");
                reply = UUID.getFromBinaryObject(replyValue);
                if (reply == null) {
                    if ((required & FIELD_REPLY) != 0) {
                        LogUtils.warn("Reply UUID was required but none was found!");
                        return null;
                    } else {
                        reply = UUID.INVALID_UUID;
                    }
                }
                String subject;
                UBValue subjectValue = rootObj.get("subject");
                if (subjectValue != null && subjectValue.isString()) {
                    subject = subjectValue.asString();
                } else if ((required & FIELD_SUBJECT) != 0) {
                    LogUtils.warn("Subject was required but none was found!");
                    return null;
                } else {
                    subject = "";
                }
                String body;
                UBValue bodyValue = rootObj.get("body");
                if (bodyValue != null && bodyValue.isString()) {
                    body = bodyValue.asString();
                } else if ((required & FIELD_BODY) != 0) {
                    LogUtils.warn("Main body was required but none was found!");
                    return null;
                } else {
                    body = "";
                }
                Map<String, Attachment> attachments;
                UBValue attachmentsValue = rootObj.get("attachments");
                attachments = Attachment.getFromBinaryMap(attachmentsValue);
                if (attachments == null) {
                    if ((required & FIELD_ATTACHMENTS) != 0) {
                        LogUtils.warn("Attachments were required but none was found!");
                        return null;
                    } else {
                        attachments = new TreeMap<>();
                    }
                }
                return new Email(uuid, isNew, sender, receiver, timestamp, reply, subject, body,
                    attachments);
            } else {
                return null;
            }
        }

        /**
         * Gets a draft and prepares it for sending. Server Side.
         */
        public static Email prepareSend(Email draft, User sender, UUID uuid) {
            long timestamp = Instant.now().toEpochMilli();
            if (draft.getReceiver() != null && draft.getReceiver().getUsername()
                .equals(sender.getUsername())) {
                LogUtils.warn(String
                    .format("Invalid receiver(%s) for new email!", draft.getReceiver().getUsername()));
                return null;
            }
            Email email = new Email(uuid, true,
                sender, draft.receiver, timestamp, draft.reply,
                draft.subject, draft.body, draft.attachments);
            return email.normalize();
        }

        /**
         * Creates a draft and prepares it for sending. Client Side. Attachment-less version.
         */
        public static Email createDraft(User receiver, UUID reply, String subject, String body) {
            if (reply == null) {
                reply = UUID.INVALID_UUID;
            }
            return new Email(null, false, null, receiver, -1, reply, subject, body, new TreeMap<>());
        }
    }
}
