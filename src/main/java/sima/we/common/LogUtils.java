package sima.we.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Log4j proxy class.
 *
 * @author Anastasios Symeonidis
 */
public class LogUtils {

    private static final Logger LOGGER = LogManager.getLogger("WonE");

    public static <T> void trace(T msg) {
        getLogger().trace(msg);
    }

    public static <T> void debug(T msg) {
        getLogger().debug(msg);
    }

    public static <T> void info(T msg) {
        getLogger().info(msg);
    }

    public static <T> void warn(T msg) {
        getLogger().warn(msg);
    }

    public static <T> void error(T msg) {
        getLogger().error(msg);
    }

    public static <T> void fatal(T msg) {
        getLogger().fatal(msg);
    }

    public static Logger getLogger() {
        return LOGGER;
    }
}
