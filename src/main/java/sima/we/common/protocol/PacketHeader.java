package sima.we.common.protocol;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import sima.we.common.LogUtils;
import sima.we.common.protocol.payloads.PayloadType;

/**
 * Tools to create and extract information from the network header.
 *
 * @author Anastasios Symeonidis
 */
public class PacketHeader {

    // From specification.
    public static final byte[] PROTOCOL_IDENTIFIER = "WE".getBytes(StandardCharsets.US_ASCII);
    public static final byte[] PROTOCOL_VERSION = {0x00, 0x00};
    public static final int LENGTH = 0xc;

    private final byte[] packetID;
    private final byte[] packetVersion;
    private final Compression flagsCompression;
    private final PayloadType payloadType;
    private final int dataLength;

    /**
     * Internal constructor. Must always be the last call in a constructor.
     */
    protected PacketHeader(byte[] id, byte[] version, Compression comp, PayloadType msg, int len) {
        this.packetID = id;
        this.packetVersion = version;
        this.flagsCompression = comp;
        this.payloadType = msg;
        this.dataLength = len;
    }

    /**
     * Constructs the default header for the specified payload and compression. The result is always a
     * valid.
     *
     * @param payloadLength payload length after the compression.
     */
    public PacketHeader(Compression comp, PayloadType type, int payloadLength) {
        this(PROTOCOL_IDENTIFIER, PROTOCOL_VERSION, comp, type, payloadLength);
        if (comp == null) {
            throw new IllegalArgumentException("comp cannot be null!");
        }
        if (type == null) {
            throw new IllegalArgumentException("type cannot be null!");
        }
        if (payloadLength < 0) {
            throw new IllegalArgumentException("payloadLength cannot be negative!");
        }
    }

    /**
     * Constructs the default header for the specified payload/message. The result is always a valid.
     */
    public PacketHeader(PayloadType type, int payloadLength) {
        this(Compression.NONE, type, payloadLength);
    }

    /**
     * Converts binary data to a {@link PacketHeader}.
     *
     * @param buffer received data.
     */
    public static PacketHeader readFromStream(byte[] buffer) {
        // Get raw values.
        byte[] id = new byte[]{buffer[0], buffer[1]};
        byte[] version = new byte[]{buffer[2], buffer[3]};
        short flags = (short) ((buffer[4] & 0xff) << 8 | buffer[5] & 0xff);
        short typeValue = (short) ((buffer[6] & 0xff) << 8 | buffer[7] & 0xff);
        int length = (buffer[8] & 0xFF) << 24 | (buffer[9] & 0xFF) << 16 |
            (buffer[10] & 0xFF) << 8 | buffer[11] & 0xFF;
        // Process raw values.
        Compression flagComp = Compression.getFromFlag(flags);
        PayloadType type = PayloadType.getFromValue(typeValue);
        if (type == null) {
            LogUtils.warn(String.format(
                "Payload id %x does not match any known payload types!", typeValue));
        }
        // Initialize object.
        return new PacketHeader(id, version, flagComp, type, length);
    }

    /**
     * Converts binary data to a {@link PacketHeader}.
     *
     * @param is input
     */
    public static PacketHeader readFromStream(InputStream is) throws IOException {
        byte[] receiveBuffer = new byte[LENGTH];
        int received = 0;
        while (received < LENGTH) {
            int read = is.read(receiveBuffer, received, LENGTH - received);
            if (read == -1) {
                throw new IOException("Stream closed while reading header!");
            }
            received += read;
        }
        return readFromStream(receiveBuffer);
    }

    /**
     * Checks if the header contains valid data. Useful only if this packet object was created with
     * readFromStream.
     */
    public boolean verify() {
        if (!Arrays.equals(packetID, PROTOCOL_IDENTIFIER)) {
            LogUtils.debug("Header ID is invalid!");
            return false;
        }
        if (!Arrays.equals(packetVersion, PROTOCOL_VERSION)) {
            LogUtils.debug("Header Version is invalid!");
            return false;
        }
        if (flagsCompression == null) {
            LogUtils.debug("Header compression is missing or invalid!");
            return false;
        }
        if (payloadType == null) {
            LogUtils.debug("Header payloadType is missing or invalid!");
            return false;
        }
        return true;
    }

    /**
     * @return the binary representation of this header.
     * <p>
     * Note: does not check the validity of the header.
     */
    public byte[] bytes() {
        byte[] ret = new byte[LENGTH];
        // 'WE'
        ret[0] = packetID[0];
        ret[1] = packetID[1];
        // Version
        ret[2] = packetVersion[0];
        ret[3] = packetVersion[1];
        // flags
        int flags = flagsCompression.level;
        ret[4] = (byte) ((flags >> 8) & 0xff);
        ret[5] = (byte) (flags & 0xff);
        // payload type
        int payload = payloadType.getId();
        ret[6] = (byte) ((payload >> 8) & 0xff);
        ret[7] = (byte) (payload & 0xff);
        // length
        ret[8] = (byte) ((dataLength >> 24) & 0xff);
        ret[9] = (byte) ((dataLength >> 16) & 0xff);
        ret[10] = (byte) ((dataLength >> 8) & 0xff);
        ret[11] = (byte) (dataLength & 0xff);
        return ret;
    }

    /**
     * Given a decoded {@link PacketHeader} and an InputStream get the raw uncompressed byte stream
     * containing the payload.
     *
     * @param is input
     * @return raw uncompressed data or null if the received data were corrupted.
     */
    public byte[] getPayloadData(InputStream is) throws IOException {
        LogUtils.debug("Receiving " + dataLength + " payload bytes!");
        // DOS WARNING: dataLength is user controlled!!!
        byte[] receiveBuffer = new byte[dataLength];
        int received = 0;
        while (received < dataLength) {
            int read = is.read(receiveBuffer, received, dataLength - received);
            if (read == -1) {
                throw new IOException("Stream closed while reading payload!");
            }
            received += read;
        }
        byte[] decompressed = flagsCompression.decompress(receiveBuffer);
        if (decompressed != null) {
            LogUtils.debug("Received " + decompressed.length + " payload bytes after decompression!");
        } else {
            LogUtils.warn("Couldn't decompress payload!");
        }
        return decompressed;
    }

    public Compression getCompression() {
        return flagsCompression;
    }

    public PayloadType getPayloadType() {
        return payloadType;
    }

    public int getPayloadLength() {
        return dataLength;
    }

    public enum Compression {
        NONE(0b00),
        ZLIB(0b01);

        /**
         * Bit mask to apply to `flags` Must not contain zero in between ones.
         */
        public static final int FLAGS_COMPRESSION_MASK = 0b11;

        protected static Compression[] bitFieldMapping;
        protected final int level;

        Compression(int level) {
            this.level = level;
        }

        /**
         * @param flag Packet field.
         * @return enum or null if value in field invalid.
         */
        public static Compression getFromFlag(int flag) {
            if (bitFieldMapping == null) {
                bitFieldMapping = new Compression[FLAGS_COMPRESSION_MASK + 1];
                for (Compression comp : values()) {
                    bitFieldMapping[comp.level] = comp;
                }
            }
            flag &= FLAGS_COMPRESSION_MASK;
            return bitFieldMapping[flag];
        }

        /**
         * Converts indexes of the handshake payload to Compression enums.
         */
        public static Compression[] index2Enum(short[] indexes) {
            Compression[] ret = new Compression[indexes.length];
            for (int i = 0; i < indexes.length; i++) {
                if (indexes[i] < 0 || indexes[i] > values().length) {
                    return null;
                }
                ret[i] = Compression.values()[indexes[i]];
            }
            return ret;
        }

        /**
         * Converts Compression enums to indexes of the handshake payload.
         */
        public static short[] enum2Index(Compression[] enums) {
            short[] ret = new short[enums.length];
            for (int i = 0; i < enums.length; i++) {
                ret[i] = (short) enums[i].ordinal();
            }
            return ret;
        }

        /**
         * Given a byte stream return the uncompressed version of it given this {@link Compression}
         * method.
         *
         * @param data potentially compressed data.
         * @return always decompressed data.
         */
        public byte[] decompress(byte[] data) {
            switch (this) {
                case NONE:
                    return data;
                case ZLIB:
                    Inflater deComp = new Inflater();
                    deComp.setInput(data);
                    // Java doesn't have a lot of good resizable byte buffers...
                    try (ByteArrayOutputStream outputData = new ByteArrayOutputStream()) {
                        byte[] buffer = new byte[1024];
                        while (deComp.getRemaining() > 0) {
                            // Decompression happens here.
                            int uncompressedCount = deComp.inflate(buffer);
                            // Avoid potential dos(results in infinite loop).
                            if (uncompressedCount == 0) {
                                throw new DataFormatException("Incomplete data!");
                            }
                            // Append uncompressed data.
                            outputData.write(buffer, 0, uncompressedCount);
                        }
                        if (deComp.finished()) {
                            return outputData.toByteArray();
                        } else {
                            // Do NOT accept incomplete ZLIB stream.
                            throw new DataFormatException("Incomplete data!");
                        }
                    } catch (DataFormatException | IOException e) {
                        LogUtils.getLogger().error("Corrupted payload packet!", e);
                        return null;
                    }
            }
            return null;
        }

        /**
         * Try to apply compression to a byte array.
         *
         * @param dest Output
         * @param src  Input
         * @return the length of the compressed data.
         */
        public int tryCompress(byte[] dest, byte[] src) {
            switch (this) {
                case NONE:
                    // NOP
                    return 0;
                case ZLIB:
                    // Settings
                    Deflater comp = new Deflater(7);
                    comp.setInput(src);
                    comp.finish();
                    int zLibLength = comp.deflate(dest);
                    if (comp.finished()) {
                        // Compressed data fits in destination buffer.
                        return zLibLength;
                    }
                    if (zLibLength != src.length) {
                        LogUtils.debug(
                            "ZLIB.tryCompress failed with " + zLibLength + " bytes written!"
                        );
                    }
                    return 0;
            }
            return -1;
        }
    }

}
