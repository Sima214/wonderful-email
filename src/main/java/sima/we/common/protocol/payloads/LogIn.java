package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `logIn` payload
 *
 * @author Anastasios Symeonidis
 */
public class LogIn {

    public static class Request extends PacketPayloadRequest {

        protected String username;
        protected String password;

        /**
         * Creates a log in request ready to be sent.
         *
         * @param username User input.
         * @param password User input.
         */
        public Request(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public Request() {
        }

        @Override
        public boolean parse(byte[] raw) {
            username = null;
            password = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue username = rootObj.get("username");
                    if (username != null && username.isString()) {
                        this.username = username.asString();
                    } else {
                        return false;
                    }
                    UBValue password = rootObj.get("password");
                    if (password != null && password.isString()) {
                        this.password = password.asString();
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("logIn/register request decode error!", e);
                return false;
            }
            return username != null && password != null;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            return new Response(state.tryLogIn(iss, username, password));
        }

        @Override
        public byte[] bytes() {
            if (username == null || password == null) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("username", UBValueFactory.createString(username));
                root.put("password", UBValueFactory.createString(password));
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("logIn/register request encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.LOGIN;
        }
    }

    public static class Response extends PacketPayloadResponse {

        protected boolean accepted;
        protected String message;

        public Response(boolean accepted, String message) {
            this.accepted = accepted;
            this.message = message;
        }

        /**
         * Creates a response with a default message.
         */
        public Response(boolean accepted) {
            this(accepted, accepted ? "Welcome back!" : "Invalid username or password!");
        }

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            accepted = false;
            message = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue accepted = rootObj.get("accepted");
                    if (accepted != null && accepted.isBool()) {
                        this.accepted = accepted.asBool();
                    } else {
                        return false;
                    }
                    UBValue message = rootObj.get("message");
                    if (message != null && (message.isString() || message.isNull())) {
                        this.message = message.asString();
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("logIn/register response decode error!", e);
                return false;
            }
            return true;
        }

        @Override
        public byte[] bytes() {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("accepted", UBValueFactory.createBool(accepted));
                if (message != null) {
                    root.put("message", UBValueFactory.createString(message));
                }
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("logIn/register response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.LOGIN;
        }

        public boolean isAccepted() {
            return accepted;
        }

        public String getMessage() {
            return message;
        }
    }

}
