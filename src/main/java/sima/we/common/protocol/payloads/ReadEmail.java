package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.Email;
import sima.we.common.Email.UUID;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `readEmail` payload
 *
 * @author Anastasios Symeonidis
 */
public class ReadEmail {

    public static class Request extends PacketPayloadRequest {

        private UUID emailUUID;

        public Request() {
        }

        public Request(UUID emailUUID) {
            this.emailUUID = emailUUID;
        }

        @Override
        public boolean parse(byte[] raw) {
            emailUUID = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    emailUUID = UUID.getFromBinaryObject(rootObj.get("uuid"));
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("readEmail request decode error!", e);
                return false;
            }
            return emailUUID != null;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            return new Response(state.getEmail(iss, emailUUID));
        }

        @Override
        public byte[] bytes() {
            if (emailUUID == null) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("uuid", emailUUID.getAsBinaryObject());
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("readEmail encode decode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.READ_EMAIL;
        }
    }

    public static class Response extends PacketPayloadResponse {

        private Email email;

        public Response(Email email) {
            this.email = email;
        }

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                email = Email.Factory.get(root, Email.FIELD_FULL);
            } catch (IOException e) {
                LogUtils.getLogger().error("readEmail response decode error!", e);
                return false;
            }
            return true;
        }

        @Override
        public byte[] bytes() {
            if (email == null || email.isPartial()) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBValue root = email.getAsBinaryObject(Email.FIELD_FULL);
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("readEmail response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.READ_EMAIL;
        }

        public Email getEmail() {
            return email;
        }
    }
}
