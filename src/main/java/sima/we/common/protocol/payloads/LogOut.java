package sima.we.common.protocol.payloads;

import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `logIn` payload
 *
 * @author Anastasios Symeonidis
 */
public class LogOut {

    public static class Request extends PacketPayloadRequest {

        public Request() {
        }

        @Override
        public boolean parse(byte[] raw) {
            return raw.length == 0;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            state.logOut(iss);
            return new Response();
        }

        @Override
        public byte[] bytes() {
            return new byte[0];
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.LOG_OUT;
        }
    }

    public static class Response extends PacketPayloadResponse {

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            return raw.length == 0;
        }

        @Override
        public byte[] bytes() {
            return new byte[0];
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.LOG_OUT;
        }
    }
}
