package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.Email;
import sima.we.common.Email.UUID;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `newEmail` payload
 *
 * @author Anastasios Symeonidis
 */
public class NewEmail {

    public static class Request extends PacketPayloadRequest {

        private Email email;

        public Request() {
        }

        public Request(Email email) {
            if (email.isPartial()) {
                throw new IllegalArgumentException("Argument email can not be partial!");
            }
            this.email = email;
        }

        @Override
        public boolean parse(byte[] raw) {
            email = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                email = Email.Factory.get(root, Email.FIELD_NEW);
            } catch (IOException e) {
                LogUtils.getLogger().error("newEmail request decode error!", e);
                return false;
            }
            return email != null;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            Email email = state.trySendEmail(iss, this.email);
            if (email != null) {
                return new Response(email.getUUID());
            }
            return null;
        }

        @Override
        public byte[] bytes() {
            if (email == null) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                writer.write(email.getAsBinaryObject(Email.FIELD_NEW));
            } catch (IOException e) {
                LogUtils.getLogger().error("newEmail request encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.NEW_EMAIL;
        }
    }

    public static class Response extends PacketPayloadResponse {

        private boolean sent;
        private String message;
        private UUID uuid;

        /**
         * Full constructor.
         *
         * @param uuid null or invalid if the message was not sent.
         */
        public Response(UUID uuid, String message) {
            this.sent = uuid != null && uuid.isValid();
            this.message = message;
            this.uuid = uuid;
        }

        /**
         * Creates a `newEmail` response with a default message.
         *
         * @param uuid null or invalid if the message was not sent.
         */
        public Response(UUID uuid) {
            this(uuid, null);
            if (!sent) {
                this.message = "Could not send the email!";
            }
        }

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            sent = false;
            message = null;
            uuid = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue sentValue = rootObj.get("sent");
                    if (sentValue != null && sentValue.isBool()) {
                        sent = sentValue.asBool();
                    } else {
                        return false;
                    }
                    // Message is always null.
                    UBValue messageValue = rootObj.get("message");
                    if (messageValue != null && messageValue.isString()) {
                        message = messageValue.asString();
                    }
                    UBValue uuidValue = rootObj.get("uuid");
                    uuid = UUID.getFromBinaryObject(uuidValue);
                    if (sent && (uuid == null || !uuid.isValid())) {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("newEmail response decode error!", e);
                return false;
            }
            return true;
        }

        @Override
        public byte[] bytes() {
            if (sent && (uuid == null || !uuid.isValid())) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("sent", UBValueFactory.createBool(sent));
                if (message != null) {
                    root.put("message", UBValueFactory.createString(message));
                }
                if (sent) {
                    root.put("uuid", uuid.getAsBinaryObject());
                }
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("newEmail response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.NEW_EMAIL;
        }

        public boolean isSent() {
            return sent;
        }

        public String getMessage() {
            return message;
        }

        public UUID getUuid() {
            return uuid;
        }
    }
}
