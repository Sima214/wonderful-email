package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.Account.Inbox;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `showEmails` payload
 *
 * @author Anastasios Symeonidis
 */
public class ShowEmails {

    public static class Request extends PacketPayloadRequest {

        public Request() {
        }

        @Override
        public boolean parse(byte[] raw) {
            return raw.length == 0;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            return new Response(state.getInbox());
        }

        @Override
        public byte[] bytes() {
            return new byte[0];
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.SHOW_EMAILS;
        }
    }

    public static class Response extends PacketPayloadResponse {

        private Inbox list;

        /**
         * Gets and stores an internal deep copy of an inbox. Used by the server code.
         */
        public Response(Inbox inbox) {
            list = inbox.partialCopy();
        }

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            list = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue inbox = rootObj.get("inbox");
                    list = Inbox.getFromPartialBinaryObject(inbox);
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("showEmails response decode error!", e);
                return false;
            }
            return list != null;
        }

        @Override
        public byte[] bytes() {
            if (list == null) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("inbox", list.getAsPartialBinaryObject());
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("showEmails response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.SHOW_EMAILS;
        }

        public Inbox getInbox() {
            return list;
        }
    }
}
