package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.Email.UUID;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `deleteEmail` payload
 *
 * @author Anastasios Symeonidis
 */
public class DeleteEmail {

    public static class Request extends PacketPayloadRequest {

        private UUID uuid;

        public Request(UUID uuid) {
            this.uuid = uuid;
        }

        public Request() {
        }

        @Override
        public boolean parse(byte[] raw) {
            uuid = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue uuidValue = rootObj.get("uuid");
                    uuid = UUID.getFromBinaryObject(uuidValue);
                    return uuid != null;
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("deleteEmail request decode error!", e);
                return false;
            }
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            return new Response(state.tryDeleteEmail(iss, uuid));
        }

        @Override
        public byte[] bytes() {
            if (uuid == null) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("uuid", uuid.getAsBinaryObject());
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("deleteEmail request encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.DELETE_EMAIL;
        }
    }

    public static class Response extends PacketPayloadResponse {

        private boolean deleted;

        public Response(boolean deleted) {
            this.deleted = deleted;
        }

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue deletedValue = rootObj.get("deleted");
                    if (deletedValue != null && deletedValue.isBool()) {
                        deleted = deletedValue.asBool();
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("deleteEmail response decode error!", e);
                return false;
            }
        }

        @Override
        public byte[] bytes() {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("deleted", UBValueFactory.createBool(deleted));
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("deleteEmail response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.DELETE_EMAIL;
        }

        public boolean isDeleted() {
            return deleted;
        }
    }
}
