package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBArray;
import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.LogUtils;
import sima.we.common.User;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `listUsers` payload
 *
 * @author Anastasios Symeonidis
 */
public class ListUsers {

    public static class Request extends PacketPayloadRequest {

        public Request() {
        }

        @Override
        public boolean parse(byte[] raw) {
            return raw.length == 0;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            if (state.requireAuthentication()) {
                return new Response(iss.getUserList());
            }
            return null;
        }

        @Override
        public byte[] bytes() {
            return new byte[0];
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.LIST_USERS;
        }
    }

    public static class Response extends PacketPayloadResponse {

        private User[] userList;

        public Response(User[] userList) {
            this.userList = userList;
        }

        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            userList = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue userList = rootObj.get("user_list");
                    if (userList != null && userList.isArray()) {
                        UBArray userArray = userList.asArray();
                        int len = userArray.size();
                        User[] newUserList = new User[len];
                        for (int i = 0; i < len; i++) {
                            User newUser = User.getFromBinaryObject(userArray.get(i));
                            if (newUser != null) {
                                newUserList[i] = newUser;
                            } else {
                                return false;
                            }
                        }
                        this.userList = newUserList;
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("listUsers response decode error!", e);
                return false;
            }
        }

        @Override
        public byte[] bytes() {
            if (userList == null) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                // First generate an array of user_obj(the ubjson kind).
                UBValue[] userArray = new UBValue[userList.length];
                for (int i = 0; i < userList.length; i++) {
                    userArray[i] = userList[i].getAsBinaryObject();
                }
                root.put("user_list", UBValueFactory.createArray(userArray));
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("listUsers response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.LIST_USERS;
        }

        public User[] getUsers() {
            return userList;
        }
    }
}
