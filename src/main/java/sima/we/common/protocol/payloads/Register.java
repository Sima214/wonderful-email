package sima.we.common.protocol.payloads;

import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.server.store.IServerStore;

/**
 * `register` payload
 *
 * @author Anastasios Symeonidis
 */
public class Register {

    public static class Request extends LogIn.Request {

        /**
         * Creates a register request ready to be sent.
         *
         * @param username User input.
         * @param password User input.
         */
        public Request(String username, String password) {
            super(username, password);
        }

        public Request() {
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            return new Response(state.tryRegister(iss, username, password));
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.REGISTER;
        }
    }

    public static class Response extends LogIn.Response {

        public Response() {
        }

        public Response(boolean accepted) {
            super(accepted, accepted ? "Welcome!" : "Could not register new user!");
        }

        public Response(boolean b, String s) {
            super(b, s);
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.REGISTER;
        }
    }

}
