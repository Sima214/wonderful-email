package sima.we.common.protocol.payloads;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBValueFactoryEx;
import com.devsmart.ubjson.UBWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.PacketHeader.Compression;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;
import sima.we.server.store.IServerStore;

/**
 * `handshake` payload
 *
 * @author Anastasios Symeonidis
 */
public class Handshake {

    public static class Request extends PacketPayloadRequest {

        private Compression[] compressionTypes;

        /**
         * Creates a handshake request with all supported compression types.
         */
        public Request() {
            compressionTypes = Compression.values();
        }

        @Override
        public boolean parse(byte[] raw) {
            this.compressionTypes = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                // Note: we do not check if the client has sent extra data than the root object, which is not mentioned in the spec.
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue compressionTypes = rootObj.get("compression_types");
                    if (compressionTypes.isArray()) {
                        this.compressionTypes = Compression
                            .index2Enum(compressionTypes.asShortArray());
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("Handshake request decode error!", e);
                return false;
            }
            return compressionTypes != null;
        }

        @Override
        public PacketPayloadResponse buildResponse(Packet packet, ConnectionState state,
            IServerStore iss) {
            return new Response(state.handshake(compressionTypes, Compression.values()));
        }

        @Override
        public byte[] bytes() {
            if (compressionTypes == null || compressionTypes.length == 0) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("compression_types",
                    UBValueFactory.createArray(Compression.enum2Index(compressionTypes)));
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("Handshake request encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.HANDSHAKE;
        }
    }

    public static class Response extends PacketPayloadResponse {

        private boolean accepted;
        private Compression compression;
        private String message;

        /**
         * Creates a new handshake response with the specified message.
         */
        public Response(Compression acceptedCompression, String msg) {
            this.accepted = acceptedCompression != null;
            this.compression = acceptedCompression;
            this.message = msg;
        }

        /**
         * Creates a new handshake response with a default message.
         *
         * @param acceptedCompression The accepted compression method, or null if there was no match.
         */
        public Response(Compression acceptedCompression) {
            this(acceptedCompression,
                acceptedCompression != null ? "Welcome!" : "Handshake error!");
        }

        /**
         * Empty constructor. Resulting object only supports parse.
         */
        public Response() {
        }

        @Override
        public boolean parse(byte[] raw) {
            accepted = false;
            compression = null;
            message = null;
            try (UBReader reader = new UBReader(new ByteArrayInputStream(raw))) {
                // Note: we do not check if the client has sent extra data than the root object, which is not mentioned in the spec.
                UBValue root = reader.read();
                if (root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue accepted = rootObj.get("accepted");
                    if (accepted != null && accepted.isBool()) {
                        this.accepted = accepted.asBool();
                    } else {
                        return false;
                    }
                    UBValue compression = rootObj.get("compression_type");
                    if (this.accepted && compression != null && compression.isInteger()) {
                        this.compression = Compression.values()[compression.asShort()];
                    } else if (!this.accepted && (compression == null || compression.isNull())) {
                        this.compression = null;
                    } else {
                        return false;
                    }
                    UBValue message = rootObj.get("message");
                    if (message != null && message.isString()) {
                        this.message = message.asString();
                    } else {
                        this.message = null;
                    }
                } else {
                    return false;
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("Handshake response decode error!", e);
                return false;
            }
            return true;
        }

        @Override
        public byte[] bytes() {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try (UBWriter writer = new UBWriter(buffer)) {
                UBObject root = UBValueFactory.createObject();
                root.put("accepted", UBValueFactory.createBool(accepted));
                if (message != null) {
                    root.put("message", UBValueFactory.createString(message));
                }
                if (compression != null) {
                    root.put("compression_type", UBValueFactoryEx.createInt16(
                        (short) compression.ordinal()));
                }
                writer.write(root);
            } catch (IOException e) {
                LogUtils.getLogger().error("Handshake response encode error!", e);
                return null;
            }
            return buffer.toByteArray();
        }

        @Override
        public PayloadType getPayloadType() {
            return PayloadType.HANDSHAKE;
        }

        public boolean isAccepted() {
            return accepted;
        }

        public Compression getCompression() {
            return compression;
        }

        public String getMessage() {
            return message;
        }
    }
}
