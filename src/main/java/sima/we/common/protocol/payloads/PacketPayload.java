package sima.we.common.protocol.payloads;

import java.io.IOException;
import java.io.InputStream;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.PacketHeader;
import sima.we.server.store.IServerStore;

/**
 * Collection of tools to create and parse payloads.
 *
 * @author Anastasios Symeonidis
 */
public abstract class PacketPayload {

    /**
     * Receive a {@link PacketPayload}.
     *
     * @param is     IO stream where we receive data
     * @param header header of this payload
     * @param server true if this is called from server code, false otherwise
     * @return Decoded received data
     */
    public static PacketPayload readFromStream(
        InputStream is, PacketHeader header, boolean server) throws IOException {
        byte[] rawData = header.getPayloadData(is);
        if (rawData == null) {
            // Corrupted data.
            LogUtils.debug("Received corrupted payload data!");
            return null;
        }
        PacketPayload payload;
        if (server) {
            payload = header.getPayloadType().newRequest();
        } else { // client
            payload = header.getPayloadType().newResponse();
        }
        if (payload.parse(rawData)) {
            return payload;
        } else {
            LogUtils.debug("Couldn't parse payload data!");
            return null;
        }
    }

    public abstract boolean parse(byte[] raw);

    public abstract byte[] bytes();

    public abstract PayloadType getPayloadType();

    /**
     * What the client sends to the server.
     */
    public abstract static class PacketPayloadRequest extends PacketPayload {

        /**
         * Parses a client request.
         *
         * @param raw raw uncompressed bytes
         * @return true if parsing was successful.
         */
        @Override
        public abstract boolean parse(byte[] raw);

        /**
         * Builds/Prepares a response to a client
         *
         * @param packet header and this payload
         * @param state  The connection state for this client. Tracks state between requests.
         * @param iss    Server data state.
         */
        public abstract PacketPayloadResponse buildResponse(Packet packet,
            ConnectionState state, IServerStore iss);

        /**
         * @return Returns the binary representation of this request.
         */
        @Override
        public abstract byte[] bytes();
    }

    /**
     * What the server sends to the client.
     */
    public abstract static class PacketPayloadResponse extends PacketPayload {

        /**
         * Parses a server response.
         *
         * @param raw raw uncompressed bytes
         * @return true if parsing was successful.
         */
        @Override
        public abstract boolean parse(byte[] raw);

        /**
         * @return Returns the binary representation of this response.
         */
        @Override
        public abstract byte[] bytes();

    }
}
