package sima.we.common.protocol;

import sima.we.common.Account;
import sima.we.common.Account.Inbox;
import sima.we.common.Email;
import sima.we.common.Email.UUID;
import sima.we.common.LogUtils;
import sima.we.common.protocol.PacketHeader.Compression;
import sima.we.server.store.IServerStore;

/**
 * Used by the server to track, handle and respond to client requests.
 *
 * @author Anastasios Symeonidis
 */
public class ConnectionState {

    private States state;
    private Compression compression;
    private Account authenticatedUser;

    /**
     * Constructs a new Automaton.
     */
    public ConnectionState() {
        state = States.NEW;
        compression = Compression.NONE;
        authenticatedUser = null;
    }

    /**
     * Try to authenticate a user.
     */
    protected boolean authenticate(IServerStore iss, String username, String password) {
        authenticatedUser = iss.logIn(username, password);
        if (authenticatedUser != null) {
            state = States.AUTHENTICATED;
            return true;
        }
        return false;
    }

    /**
     * Does the reverse of {@link #authenticate(IServerStore, String, String)}
     */
    protected void unauthenticate(IServerStore iss) {
        iss.logOut(authenticatedUser);
        authenticatedUser = null;
        state = States.UNAUTHENTICATED;
    }

    /**
     * Decides and registers the compression method to be used for communication.
     * <p>
     * Both lists are assumed to be in order of preference.
     * <p>
     * `NONE` compression type is ignored.
     *
     * @param clientCompression List of compression methods advertised from the client.
     * @param serverCompression List of compression methods supported on this server.
     * @return the decided {@link Compression}.
     */
    public Compression handshake(Compression[] clientCompression, Compression[] serverCompression) {
        // Check for invalid state.
        if (state != States.NEW) {
            LogUtils.error("Tried handshake while on " + state.name());
            state = States.CLOSED;
            return null;
        }
        // Decide on compression.
        for (Compression s : serverCompression) {
            for (Compression c : clientCompression) {
                if (s != Compression.NONE && s == c) {
                    compression = s;
                    LogUtils.info("Using " + compression.name() + " compression");
                }
            }
        }
        // Change state.
        state = States.UNAUTHENTICATED;
        return compression;
    }

    /**
     * @param iss      Access to server store.
     * @param username User provided string.
     * @param password User provided string.
     * @return true if the log in was a success and the client now is authenticated.
     */
    public boolean tryLogIn(IServerStore iss, String username, String password) {
        // Invalid state check.
        if (state != States.UNAUTHENTICATED) {
            LogUtils.error("Tried logIn while on " + state.name());
            state = States.CLOSED;
            return false;
        }
        return authenticate(iss, username, password);
    }

    /**
     * @param iss      Access to server store.
     * @param username User provided string.
     * @param password User provided string.
     * @return true if the register was a success and the client now is authenticated.
     */
    public boolean tryRegister(IServerStore iss, String username, String password) {
        // Invalid state check.
        if (state != States.UNAUTHENTICATED) {
            LogUtils.error("Tried register while on " + state.name());
            state = States.CLOSED;
            return false;
        }
        return iss.addUser(username, password) && authenticate(iss, username, password);
    }

    /**
     * Security check that the user is authenticated/logged in.
     *
     * @return true if the client is in authenticated state.
     */
    public boolean requireAuthentication() {
        if (state != States.AUTHENTICATED) {
            LogUtils.error("Requiring authenticated state while on " + state.name());
            state = States.CLOSED;
            return false;
        }
        return true;
    }

    /**
     * @return the inbox of the authenticated user, or null.
     */
    public Inbox getInbox() {
        if (requireAuthentication()) {
            return authenticatedUser.getInbox();
        }
        return null;
    }

    /**
     * @param iss   Server Store State
     * @param email The email-draft to send.
     * @return the sent email.
     */
    public Email trySendEmail(IServerStore iss, Email email) {
        if (requireAuthentication()) {
            return iss.sendEmail(authenticatedUser, email);
        }
        return null;
    }

    /**
     * Retrieves an email which this authenticated {@link sima.we.common.User} has access to, while also
     * clearing the isNew flag.
     *
     * @param iss       Server Store State
     * @param emailUUID ID of the email the client wants.
     * @return The email or null if the requested email does exist, or current user does not have access
     * to it.
     */
    public Email getEmail(IServerStore iss, UUID emailUUID) {
        if (requireAuthentication()) {
            Email email = authenticatedUser.getInbox().find(emailUUID);
            if (email != null && email.isNew()) {
                email = iss.markAsRead(authenticatedUser, email);
            }
            return email;
        }
        return null;
    }

    /**
     * @param iss  Server Store State
     * @param uuid ID of the email the client wants to delete.
     * @return true if an actual email got deleted.
     */
    public boolean tryDeleteEmail(IServerStore iss, UUID uuid) {
        if (requireAuthentication()) {
            return iss.deleteEmail(authenticatedUser, uuid);
        }
        return false;
    }

    /**
     * De-authenticate a potentially authenticated client. This method never fails.
     *
     * @param iss Server store state.
     */
    public void logOut(IServerStore iss) {
        if (state == States.AUTHENTICATED) {
            unauthenticate(iss);
            state = States.UNAUTHENTICATED;
        }
    }

    /**
     * Releases any potential unreleased resources.
     */
    public void close(IServerStore iss) {
        state = States.CLOSED;
        if (authenticatedUser != null) {
            unauthenticate(iss);
        }
    }

    /**
     * Whether we've reached a state where the Server-Client connection must close.
     */
    public boolean isClosedState() {
        return state == States.CLOSED;
    }

    /**
     * @return the compression methods decided upon the handshake.
     */
    public Compression getCompression() {
        return compression;
    }

    private enum States {
        NEW, UNAUTHENTICATED, AUTHENTICATED, CLOSED;
    }
}
