package sima.we.common.protocol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import sima.we.common.LogUtils;
import sima.we.common.protocol.PacketHeader.Compression;
import sima.we.common.protocol.payloads.PacketPayload;

/**
 * A container format for PacketHeader and PacketPayload.
 *
 * @author Anastasios Symeonidis
 */
public class Packet {

    public final PacketHeader header;
    public final PacketPayload payload;

    public Packet(PacketHeader h, PacketPayload p) {
        header = h;
        payload = p;
    }

    /**
     * Given a payload and a compression method, create a valid header and send the data over an output
     * stream.
     *
     * @param os          output.
     * @param payload     the payload.
     * @param compression valid compression method.
     * @return true if data was successfully sent.
     */
    public static boolean send(OutputStream os, PacketPayload payload, Compression compression)
        throws IOException {
        // Try to compress.
        byte[] responseData = payload.bytes();
        if (responseData == null) {
            LogUtils.error("Cannot encode payload!");
            return false;
        }
        byte[] compressedData = new byte[responseData.length];
        int compressedLength = compression.tryCompress(compressedData, responseData);
        // Generate header.
        PacketHeader header;
        if (compressedLength > 0) {
            // Compression was a success.
            header = new PacketHeader(compression, payload.getPayloadType(), compressedLength);
        } else {
            // Compression either failed, or increased the data size, or NONE compression was chosen.
            header = new PacketHeader(Compression.NONE, payload.getPayloadType(), responseData.length);
        }
        // Output.
        byte[] headerBytes = header.bytes();
        if (headerBytes == null) {
            LogUtils.error("Cannot encode header!");
            return false;
        }
        LogUtils.debug("Sending header(" + headerBytes.length + ")");
        os.write(headerBytes);
        if (compressedLength > 0) {
            LogUtils.debug("Sending compressed payload(" + compressedLength + ")");
            os.write(compressedData, 0, compressedLength);
        } else {
            LogUtils.debug("Sending payload(" + responseData.length + ")");
            os.write(responseData);
        }
        return true;
    }

    /**
     * Gets the data from an InputStream and constructs a packet, which is the combination of a header and
     * payload. Returns null if the data in the input is invalid.
     *
     * @param server True if this function is called from server code.
     */
    public static Packet read(InputStream is, boolean server) throws IOException {
        PacketHeader header = PacketHeader.readFromStream(is);
        if (header.verify()) {
            PacketPayload payload = PacketPayload.readFromStream(is, header, server);
            if (payload != null) {
                return new Packet(header, payload);
            } else {
                LogUtils.warn("Received invalid payload!");
                return null;
            }
        } else {
            LogUtils.warn("Received invalid header!");
            return null;
        }
    }
}
