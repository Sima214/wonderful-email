package sima.we.common;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import sima.we.client.Util;

/**
 * Centralized command line argument parsing.
 *
 * @author Anastasios Symeonidis
 */
public class ConfigProvider {

    public static final ConfigProvider instance = new ConfigProvider();

    private static final int DEFAULT_PORT = 20127;
    private static final String DEFAULT_KEY_STORE_PRIV = "../welocal.jks";
    private static final String DEFAULT_KEY_STORE_PUB = "../cacerts.jks";
    private static final String DEFAULT_KEY_PASS = "welocal";

    private String addressString = null;
    private String certPath = null;
    private String certPass = null;
    private int port = -1;

    private IPFilter addressFilter = null;


    private ConfigProvider() {
    }

    /**
     * Takes in command line arguments and parses them. The results can later be accessed using the
     * getters.
     *
     * @param args     command line arguments.
     * @param priv_key True if you want the keystore with the private key. Or simply: true = server
     */
    public void handleArgs(String args[], boolean priv_key) {
        ArgumentParser parser = ArgumentParsers.newFor("wonderful_email").build()
            .defaultHelp(true)
            .description("Subarashiki Denshimēru " + (priv_key ? "Server" : "Client"))
            .version("0.1");
        parser.addArgument("-a", "--address")
            .dest("address")
            .help("Client: Address to connect to.\nServer: IP address whitelist.");
        parser.addArgument("-p", "--port")
            .dest("port")
            .setDefault(DEFAULT_PORT)
            .type(Integer.class)
            .help("Port number where the service is running on.");
        parser.addArgument("-k", "--key-store")
            .dest("keyStore")
            .setDefault(priv_key ? DEFAULT_KEY_STORE_PRIV : DEFAULT_KEY_STORE_PUB)
            .help("How many threads the server spawns.");
        parser.addArgument("-s", "--key-pass")
            .dest("keyPass")
            .setDefault(DEFAULT_KEY_PASS)
            .help("Password for unlocking the key store.");
        try {
            Namespace res = parser.parseArgs(args);
            addressString = res.getString("address");
            port = res.getInt("port");
            certPath = res.getString("keyStore");
            certPass = res.getString("keyPass");
            Util.setCertificate(certPath, certPass);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(0);
        }
    }

    /**
     * Gets the ip address provided by the user at the command line. Used by the client.
     *
     * @return User provided IP address or null if no address was provided or address was invalid.
     */
    public InetAddress getIPAddress() {
        if (addressString == null || addressString.length() == 0) {
            return null;
        }
        try {
            return InetAddress.getByName(addressString);
        } catch (UnknownHostException e) {
            LogUtils.getLogger().warn("Cannot resolve provided IP address.", e);
            return null;
        }
    }

    /**
     * Filters based on the ip filter provided by the user at the command line. Used by the server.
     *
     * @param ip The client ip.
     * @return If false ignore the client.
     */
    public boolean acceptIPAddress(InetAddress ip) {
        if (addressFilter == null) {
            addressFilter = new IPFilter(addressString);
        }
        return addressFilter.accept(ip);
    }

    public String getCertificate() {
        return certPath;
    }

    public String getCertificatePassword() {
        return certPass;
    }

    /**
     * Common for Server and Client.
     *
     * @return The configured port.
     */
    public int getPort() {
        return port;
    }

    /**
     * Licensed to GraphHopper GmbH under one or more contributor license agreements. See the NOTICE file
     * distributed with this work for additional information regarding copyright ownership. GraphHopper
     * GmbH licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not
     * use this file except in compliance with the License. You may obtain a copy of the License at
     * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in
     * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
     * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
     * language governing permissions and limitations under the License.
     * <p>
     * This IP filter class accepts a list of IPs for blacklisting OR for whitelisting (but not both).
     * <p>
     * Additionally to exact match a simple wildcard expression ala 1.2.3* or 1.*.3.4 is allowed.
     * <p>
     * The internal ip filter from jetty did not work (NP exceptions)
     * <p>
     *
     * @author Peter Karich
     */
    private static class IPFilter {

        private final Set<String> whites;

        public IPFilter(String whiteList) {
            if (whiteList != null) {
                whites = createSet(whiteList.split(","));
            } else {
                whites = new HashSet<>();
            }
            if (!whites.isEmpty()) {
                LogUtils.debug("whitelist:" + whites);
            }
        }

        public boolean accept(InetAddress ip) {
            return accept(ip.getHostAddress());
        }

        public boolean accept(String ip) {
            if (whites.isEmpty()) {
                return true;
            }

            for (String w : whites) {
                if (simpleMatch(ip, w)) {
                    return true;
                }
            }
            LogUtils.warn("Did not accept IP " + ip);
            return false;
        }

        private Set<String> createSet(String[] split) {
            Set<String> set = new HashSet<>(split.length);
            for (String str : split) {
                str = str.trim();
                if (!str.isEmpty()) {
                    set.add(str);
                }
            }
            return set;
        }

        public boolean simpleMatch(String ip, String pattern) {
            String[] ipParts = pattern.split("\\*");
            for (String ipPart : ipParts) {
                int idx = ip.indexOf(ipPart);
                if (idx == -1) {
                    return false;
                }

                ip = ip.substring(idx + ipPart.length());
            }
            return true;
        }
    }
}
