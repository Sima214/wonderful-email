package sima.we;

import java.util.Arrays;
import sima.we.common.LogUtils;

/**
 * Multiplexer for client and server. Used during development cause it's faster.
 *
 * @author Anastasios Symeonidis
 */
public class Entry {

    public static void main(String[] args) {
        if (args.length >= 1) {
            String type = args[0];
            String[] subargs = Arrays.copyOfRange(args, 1, args.length);
            if (type.equals("server")) {
                sima.we.server.Entry.main(subargs);
            } else if (type.equals("client")) {
                sima.we.client.Entry.main(subargs);
            } else {
                LogUtils.warn('`' + type + "` is an invalid command line argument!");
                LogUtils.info("Please provide `server` or `client` as the first argument.");
                System.exit(1);
            }
        } else {
            LogUtils.fatal("Not enough arguments!");
            LogUtils.info("Please provide `server` or `client` as the first argument.");
            System.exit(1);
        }
    }

}
