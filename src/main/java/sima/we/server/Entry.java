package sima.we.server;

import sima.we.common.ConfigProvider;
import sima.we.common.LogUtils;

/**
 * Server initialization procedure.
 *
 * @author Anastasios Symeonidis
 */
public class Entry {

    public static void main(String[] args) {
        ConfigProvider.instance.handleArgs(args, true);
        LogUtils.info("Starting up server...");
        Server protocol = new Server(ConfigProvider.instance.getPort());
        protocol.main();
    }

}
