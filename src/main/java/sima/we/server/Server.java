package sima.we.server;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import sima.we.common.LogUtils;
import sima.we.server.store.FilesystemServerStore;
import sima.we.server.store.IServerStore;

/**
 * Handles the server lifecycle and provides methods to serve clients.
 *
 * @author Anastasios Symeonidis
 */
public class Server implements Closeable {

    /**
     * List of connected clients.
     * <p>
     * Implemented with a linked list. This way only disconnections are O(N).
     */
    private final List<ServerClient> clients;
    /**
     * User and email storage.
     */
    private IServerStore store;
    private SSLServerSocket socket;

    /**
     * @param port the port to listen to.
     */
    public Server(int port) {
        clients = Collections.synchronizedList(new LinkedList<>());
        // Create store / Load stored store.
        try {
            store = new FilesystemServerStore();
        } catch (IOException e) {
            LogUtils.getLogger().fatal("Server could not create store!", e);
            Runtime.getRuntime().exit(1);
        }
        // Start accepting connections.
        SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        try {
            socket = (SSLServerSocket) ssf.createServerSocket(port);
        } catch (IOException e) {
            LogUtils.getLogger().error(e);
        }
        // Add Ctl+C handler.
        Runtime.getRuntime().addShutdownHook(new Thread(this::close));
        LogUtils.info("Server listening at port: " + port);
    }

    /**
     * Main server loop. In each iteration it adds a new {@link ServerClient} for an incoming connection.
     */
    public void main() {
        while (!socket.isClosed()) {
            try {
                LogUtils.debug("Waiting for a new connection.");
                // Blocking call.
                SSLSocket clientSocket = (SSLSocket) socket.accept();
                // Potential race condition with removeClient.
                synchronized (clients) {
                    ServerClient newClient = new ServerClient(this, clientSocket);
                    clients.add(newClient);
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("Closing server socket...", e);
                try {
                    socket.close();
                } catch (IOException ex) {
                    LogUtils.getLogger().fatal(
                        "A fatal error occurred while closing server socket!", ex);
                }
            }
        }
    }

    /**
     * Closes all connections and stops the server.
     */
    public void close() {
        try {
            LogUtils.info("Closing connections...");
            Iterator<ServerClient> each = clients.iterator();
            while (each.hasNext()) {
                ServerClient client = each.next();
                each.remove();
                try {
                    client.close();
                } catch (IOException e) {
                    LogUtils.getLogger().error("Error when closing: " + this, e);
                }
            }
            if (!socket.isClosed()) {
                socket.close();
                LogUtils.info("Server stopped!");
            } else {
                LogUtils.info("Socket was already closed!");
            }
        } catch (IOException e) {
            LogUtils.getLogger().error(e);
        }
    }

    /**
     * Called by {@link ServerClient} instances when they are closing.
     *
     * @param client the client that got disconnected and is closing.
     */
    public void removeClient(ServerClient client) {
        if (!clients.remove(client)) {
            // Client was not in list!
            LogUtils.warn("Tried removing a non existing client!");
        }
    }

    public IServerStore getStore() {
        return store;
    }
}
