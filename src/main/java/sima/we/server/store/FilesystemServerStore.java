package sima.we.server.store;

import com.devsmart.ubjson.UBObject;
import com.devsmart.ubjson.UBReader;
import com.devsmart.ubjson.UBValue;
import com.devsmart.ubjson.UBValueFactory;
import com.devsmart.ubjson.UBWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import sima.we.common.Account;
import sima.we.common.Account.Inbox;
import sima.we.common.Email;
import sima.we.common.Email.Factory;
import sima.we.common.Email.UUID;
import sima.we.common.LogUtils;
import sima.we.common.User;

/**
 * Stores the persistent server state to a filesystem.
 *
 * @author Anastasios Symeonidis
 */
public class FilesystemServerStore implements IServerStore {

    public static final Path DEFAULT_PATH = FileSystems.getDefault().getPath("fstore");
    public static final String EMAIL_STORAGE_FILENAME = "emails";
    public static final String GLOBAL_STATE_FILENAME = "state.bin";
    public static final String USER_AUTH_FILENAME = "auth.bin";
    public static final String USER_INBOX_FILENAME = "inbox.bin";

    private final Path root;
    private final Path statePath;

    private UUID lastUUID;
    private Map<String, User> users;
    private Map<String, Account> accounts;
    private Map<String, Integer> accountsReferenceCount;

    /**
     * Creates a new {@link FilesystemServerStore} with a default path.
     */
    public FilesystemServerStore() throws IOException {
        this(DEFAULT_PATH);
    }

    /**
     * Creates a new {@link FilesystemServerStore} at the provided path.
     *
     * @param root we must have read and write access to this directory.
     */
    public FilesystemServerStore(Path root) throws IOException {
        // Validate that this is going to work...
        if (getPasswordHash("test", "4321") == null) {
            throw new RuntimeException("Runtime does not support required encryption algorithms!");
        }
        // Default values.
        lastUUID = new UUID(-1);
        users = new HashMap<>();
        accounts = new HashMap<>();
        accountsReferenceCount = new HashMap<>();
        // Cache paths.
        root = root.toAbsolutePath().normalize();
        this.root = root;
        statePath = root.resolve(GLOBAL_STATE_FILENAME);
        // 0. Ensure that the root is a directory.
        if (!Files.isDirectory(root)) {
            LogUtils.warn("Creating new FilesystemServerStore at: " + root.toString());
            Files.createDirectories(root);
        } else {
            LogUtils.info("Loading existing FilesystemServerStore from: " + root.toString());
        }
        // 0+. Ensure the email directory.
        Path emailPath = root.resolve(EMAIL_STORAGE_FILENAME);
        if (!Files.isDirectory(emailPath)) {
            LogUtils.warn("Generating email storage area...");
            Files.createDirectories(emailPath);
        }
        // 1. Restore last generated email uuid.
        loadGlobalState();
        // 2. Get user listing.
        loadUsers();
    }

    /**
     * (Re)Loads state.bin Creates a new one, if previous is corrupted or does not exist.
     */
    protected void loadGlobalState() {
        if (Files.isRegularFile(statePath)) {
            try (UBReader reader = new UBReader(Files.newInputStream(statePath))) {
                UBValue root = reader.read();
                if (root != null && root.isObject()) {
                    UBObject rootObj = root.asObject();
                    UBValue lastUUIDValue = rootObj.get("last_uuid");
                    UUID lastUUIDRestored = UUID.getFromBinaryObject(lastUUIDValue);
                    if (lastUUIDRestored != null) {
                        lastUUID = lastUUIDRestored;
                        LogUtils.info("Successfully reloaded server store state.");
                        return;
                    }
                }
            } catch (IOException e) {
                LogUtils.getLogger().error("Error reading state file!", e);
            }
        }
        // This part gets only executed when loading errors.
        saveGlobalState();
    }

    protected void saveGlobalState() {
        UBObject root = UBValueFactory.createObject();
        root.put("last_uuid", lastUUID.getAsBinaryObject());
        try (UBWriter writer = new UBWriter(Files.newOutputStream(statePath))) {
            writer.write(root);
        } catch (IOException e) {
            LogUtils.getLogger().error("Error saving state file!", e);
        }
        LogUtils.info("Successfully saved server store state.");
    }

    /**
     * ( ͡° ͜ʖ ͡ °)
     */
    protected String getPasswordHash(String salt, String pass) {
        byte[] byteSalt = salt.getBytes(StandardCharsets.UTF_8);
        KeySpec spec = new PBEKeySpec(pass.toCharArray(), byteSalt, 65536, 128);
        byte[] hash;
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = factory.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LogUtils.getLogger().fatal("Cannot create hash!", e);
            return null;
        }
        return bytes2hex(hash);
    }

    /**
     * (Re)Initializes user list.
     */
    protected void loadUsers() {
        try {
            Stream<Path> list = Files.list(root);
            list.filter(
                p -> Files.isDirectory(p)
                    && Files.isRegularFile(p.resolve(USER_AUTH_FILENAME))
                    && Files.isRegularFile(p.resolve(USER_INBOX_FILENAME))
            ).forEach(path -> {
                String username = path.getFileName().toString();
                LogUtils.info("Found user " + username);
                users.put(username, new User(username, false));
            });
        } catch (IOException e) {
            LogUtils.getLogger().fatal("Cannot load list of users!", e);
        }
    }

    protected User getUser(String username) {
        User ret;
        ret = users.get(username);
        if (ret == null) {
            accounts.get(username);
        }
        return ret;
    }

    protected Path getUserPath(String username) {
        return root.resolve(username).toAbsolutePath().normalize();
    }

    protected String getStoredPasswordHash(String username) {
        Path authPath = getUserPath(username).resolve(USER_AUTH_FILENAME);
        try (UBReader reader = new UBReader(Files.newInputStream(authPath))) {
            UBValue root = reader.read();
            if (root != null && root.isObject()) {
                UBObject rootObj = root.asObject();
                UBValue passwordHash = rootObj.get("password_hash");
                if (passwordHash != null && passwordHash.isString()) {
                    return passwordHash.asString();
                }
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("Cannot retrieve stored hash!", e);
        }
        return null;
    }

    protected int incrementReferenceCount(String username) {
        int value = accountsReferenceCount.getOrDefault(username, 0);
        value++;
        accountsReferenceCount.put(username, value);
        LogUtils.debug(String.format("Incrementing logged count for %s to %d", username, value));
        return value;
    }

    protected int decrementReferenceCount(String username) {
        int value = accountsReferenceCount.getOrDefault(username, 0);
        value--;
        LogUtils.debug(String.format("Decrementing logged count for %s to %d", username, value));
        if (value < 0) {
            throw new RuntimeException("Cannot have negative reference count!");
        } else if (value == 0) {
            accountsReferenceCount.remove(username);
        } else {
            accountsReferenceCount.put(username, value);
        }
        return value;
    }

    protected int decrementReferenceCount(Account account) {
        return decrementReferenceCount(account.getUsername());
    }

    private Path getUserPath(User user) {
        return getUserPath(user.getUsername());
    }

    /**
     * @param path  Where to store.
     * @param inbox What to store.
     * @return true if no errors occurred.
     */
    protected boolean saveInbox(Path path, Inbox inbox) {
        UBValue value = inbox.getAsBinaryObject(Email.FIELD_UUID | Email.FIELD_IS_NEW);
        if (value == null) {
            LogUtils.debug("Cannot serialize inbox!");
            return false;
        }
        try (UBWriter writer = new UBWriter(Files.newOutputStream(path))) {
            writer.write(value);
        } catch (IOException e) {
            LogUtils.getLogger().fatal("Inbox " + path.toString() + " is corrupted!", e);
            return false;
        }
        return true;
    }

    protected boolean saveInbox(String username, Inbox inbox) {
        return saveInbox(root.resolve(username).resolve(USER_INBOX_FILENAME), inbox);
    }

    protected Inbox loadInbox(String username) {
        Path inboxPath = getUserPath(username).resolve(USER_INBOX_FILENAME);
        Inbox inbox = new Inbox(null);
        try (UBReader reader = new UBReader(Files.newInputStream(inboxPath))) {
            UBValue root = reader.read();
            if (!inbox.getFromBinaryObject(root, Email.FIELD_UUID | Email.FIELD_IS_NEW)) {
                LogUtils.error("Cannot parse saved inbox for " + username);
                return null;
            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error while loading inbox for " + username, e);
            return null;
        }
        return inbox;
    }

    protected Email loadFullEmail(Email ref) {
        Path emailsPath = root.resolve(EMAIL_STORAGE_FILENAME);
        Path emailPath = emailsPath.resolve(ref.getUUID().toString() + ".bin");
        try (UBReader reader = new UBReader(Files.newInputStream(emailPath))) {
            Email email = Factory.get(reader.read(), Email.FIELD_FULL);
            if (email.getUUID().equals(ref.getUUID())) {
                email.setIsNew(ref.isNew());
                return email;
            } else {
                LogUtils.fatal("Corrupted stored email UUID!");
            }
        } catch (IOException e) {
            LogUtils.getLogger().fatal("Cannot load email: " + ref.getUUID(), e);
        }
        return ref.normalize();
    }

    @Override
    public synchronized boolean addUser(String username, String password) {
        if (getUser(username) != null) {
            LogUtils.warn("User " + username + " already exists!");
            return false;
        }
        // Create user folder and default user object.
        Path userPath = root.resolve(username);
        try {
            // Here we basically check if the username contains any characters that change the relative directory.
            userPath = userPath.toAbsolutePath().normalize();
            if (userPath.getParent().compareTo(root) != 0) {
                LogUtils.getLogger().error("Potential `register` attack detected!");
                return false;
            }
            Files.createDirectory(userPath);
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error when creating user directory.", e);
            return false;
        }
        User addedUser = new User(username, false);
        users.put(username, addedUser);
        // Write auth file.
        String hash = getPasswordHash(username, password);
        UBObject authRoot = UBValueFactory.createObject();
        authRoot.put("password_hash", UBValueFactory.createString(hash));
        Path authPath = userPath.resolve(USER_AUTH_FILENAME);
        try (UBWriter writer = new UBWriter(Files.newOutputStream(authPath))) {
            writer.write(authRoot);
        } catch (IOException e) {
            LogUtils.getLogger().warn("Couldn't create auth file!", e);
            // Reverse.
            try {
                Files.deleteIfExists(authPath);
                Files.deleteIfExists(userPath);
            } catch (IOException ex) {
                LogUtils.getLogger().fatal("Couldn't clean up after error!", ex);
            }
            return false;
        }
        // Create default inbox.
        Inbox inbox = new Inbox();
        Path inboxPath = userPath.resolve(USER_INBOX_FILENAME);
        if (!saveInbox(inboxPath, inbox)) {
            // Reverse.
            try {
                Files.deleteIfExists(inboxPath);
                Files.deleteIfExists(authPath);
                Files.deleteIfExists(userPath);
            } catch (IOException ex) {
                LogUtils.getLogger().fatal("Couldn't clean up after error!", ex);
            }
            return false;
        }
        return true;
    }

    @Override
    public synchronized Account logIn(String username, String password) {
        // Check if username exists.
        User user = getUser(username);
        if (user == null) {
            LogUtils.info("Invalid username.");
            return null;
        }
        // Validate password before continuing.
        String hash = getPasswordHash(username, password);
        String storedHash = getStoredPasswordHash(username);
        if (!hash.equals(storedHash)) {
            LogUtils.info("Invalid password.");
            return null;
        }
        // Check if user is already logged in.
        if (user instanceof Account) {
            // If so, increment reference count and return the account.
            Account account = (Account) user;
            incrementReferenceCount(username);
            return account;
        } else {
            // Else `upgrade` User to Account.
            Inbox inbox = loadInbox(username);
            if (inbox == null) {
                return null;
            } else {
                inbox.reload(this::loadFullEmail);
            }
            users.remove(username);
            Account account = new Account(user, inbox);
            accounts.put(username, account);
            incrementReferenceCount(username);
            return account;
        }
    }

    @Override
    public synchronized void logOut(Account account) {
        String username = account.getUsername();
        // Ensure that the account is actually logged in.
        if (!accounts.containsKey(username)) {
            LogUtils.warn("Tried to logout " + username + ", but he is not signed in!");
        }
        if (decrementReferenceCount(username) == 0) {
            // Downgrade Account to User.
            accounts.remove(username);
            users.put(username, new User(username, false));
        }
    }

    @Override
    public synchronized User[] getUserList() {
        Collection<User> users = this.users.values();
        Collection<Account> accounts = this.accounts.values();
        User[] r = new User[users.size() + accounts.size()];
        int i = 0;
        // Do it manually, as some transformation might be needed later.
        for (User u : users) {
            r[i] = u;
            i++;
        }
        for (User u : accounts) {
            r[i] = u;
            i++;
        }
        return r;
    }

    @Override
    public synchronized Email markAsRead(Account user, Email email) {
        email.setIsNew(false);
        Path inboxPath = getUserPath(user).resolve(USER_INBOX_FILENAME);
        saveInbox(inboxPath, user.getInbox());
        return email;
    }

    @Override
    public synchronized Email sendEmail(Account who, Email what) {
        // Create and validate the email.
        UUID nextUUID = lastUUID.increment();
        Email email = Factory.prepareSend(what, who.asUser(), nextUUID);
        if (email == null) {
            return null;
        }
        // Perform send(sync to filesystems and accounts).
        // 1. Store email.
        Path emailsPath = root.resolve(EMAIL_STORAGE_FILENAME);
        Path emailPath = emailsPath.resolve(email.getUUID().toString() + ".bin");
        UBValue emailValue = email.getAsBinaryObject(Email.FIELD_FULL);
        try (UBWriter writer = new UBWriter(Files.newOutputStream(emailPath))) {
            writer.write(emailValue);
        } catch (IOException e) {
            LogUtils.getLogger().error("Cannot add email to store!", e);
            return null;
        }
        // 2. Update receiver's inbox.
        String receiverUsername = email.getReceiver().getUsername();
        if (users.containsKey(receiverUsername)) {
            Inbox inbox = loadInbox(receiverUsername);
            inbox.emailReceived(email);
            saveInbox(receiverUsername, inbox);
        } else if (accounts.containsKey(receiverUsername)) {
            Account account = accounts.get(receiverUsername);
            account.getInbox().emailReceived(email);
            saveInbox(receiverUsername, account.getInbox());
        } else {
            LogUtils.error("New email receiver does not exist!");
            return null;
        }
        // 3. Update sender's inbox.
        who.getInbox().emailSent(email);
        saveInbox(who.getUsername(), who.getInbox());
        // 4. Update global state.
        lastUUID = nextUUID;
        saveGlobalState();
        return email;
    }

    @Override
    public synchronized boolean deleteEmail(Account user, UUID uuid) {
        LogUtils.debug(String.format("Deleting %d for user %s...", uuid.getId(), user.getUsername()));
        if (user.getInbox().delete(uuid)) {
            Path inboxPath = getUserPath(user).resolve(USER_INBOX_FILENAME);
            saveInbox(inboxPath, user.getInbox());
            LogUtils.info(String.format("Removed %d from user %s.", uuid.getId(), user.getUsername()));
            return true;
        }
        return false;
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    private static String bytes2hex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
