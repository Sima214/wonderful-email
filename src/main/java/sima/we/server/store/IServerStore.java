package sima.we.server.store;

import sima.we.common.Account;
import sima.we.common.Email;
import sima.we.common.Email.UUID;
import sima.we.common.User;

/**
 * Interface which provides a storage for emails and accounts used by the server.
 * <p>
 * ALL THE METHODS ON THIS INTERFACE MUST BE IMPLEMENTED AS SYNCHRONIZED.
 *
 * @author Anastasios Symeonidis
 */
public interface IServerStore {

    /**
     * Tries to add a new {@link User}.
     *
     * @param username Username
     * @param password Non-hashed password.
     * @return true if the registration was a success.
     */
    boolean addUser(String username, String password);

    /**
     * @param username Username
     * @param password Non-hashed password.
     * @return The authenticated account if the credentials were correct.
     */
    Account logIn(String username, String password);

    /**
     * This call must be called exactly ONCE for each successful {@link #logIn(String, String)} call.
     *
     * @param authenticatedUser the authenticated user.
     */
    void logOut(Account authenticatedUser);

    /**
     * Retrieves a list of all registered Users.
     *
     * @return array of all registered {@link User}.
     */
    User[] getUserList();

    /**
     * @param authenticatedUser the authenticated user.
     * @param email             The email to set as not new.
     * @return The email with isNew set to false.
     */
    Email markAsRead(Account authenticatedUser, Email email);

    /**
     * @param who  the authenticated user who sends the email.
     * @param what what is being sent.
     * @return what got sent, or null if nothing got sent.
     */
    Email sendEmail(Account who, Email what);

    /**
     * @param user the authenticated user who wants to delete an email.
     * @param uuid id of the email to delete.
     * @return true if there was anything to delete and user had access to it.
     */
    boolean deleteEmail(Account user, UUID uuid);
}
