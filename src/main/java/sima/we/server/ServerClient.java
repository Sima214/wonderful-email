package sima.we.server;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import javax.net.ssl.SSLSocket;
import sima.we.common.LogUtils;
import sima.we.common.protocol.ConnectionState;
import sima.we.common.protocol.Packet;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadRequest;
import sima.we.common.protocol.payloads.PacketPayload.PacketPayloadResponse;

/**
 * The representation of a connected client for the server.
 *
 * @author Anastasios Symeonidis
 */
public class ServerClient implements Runnable, Closeable {

    /**
     * Owner of this client.
     */
    private final Server server;
    /**
     * SSL tcp connection of this client.
     */
    private final SSLSocket socket;
    /**
     * Worker thread of this client.
     */
    private final Thread worker;
    /**
     * State tracking.
     */
    private final ConnectionState state;

    public ServerClient(Server server, SSLSocket clientSocket) {
        this.server = server;
        this.socket = clientSocket;
        this.worker = new Thread(this, "worker@" + this);
        this.state = new ConnectionState();
        LogUtils.info("New: " + this);
        try {
            // Setup connection options.
            socket.setKeepAlive(true);
            socket.setTcpNoDelay(true);
        } catch (SocketException e) {
            LogUtils.getLogger().error("Rejected: " + this, e);
        }
        // Start processing.
        worker.start();
    }

    @Override
    public void run() {
        try {
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            while (!socket.isClosed()) {
                Packet packet = Packet.read(is, true);
                if (packet == null) {
                    // If packet is null, then client has sent something
                    // we don't recognize, so we kick him. Like 444 on nginx.
                    LogUtils.warn("Bad Request");
                    socket.close();
                } else {
                    // Else handle request and send response.
                    PacketPayloadRequest request = (PacketPayloadRequest) packet.payload;
                    PacketPayloadResponse response = request
                        .buildResponse(packet, state, server.getStore());
                    if (state.isClosedState()) {
                        // Protocol has reached a state where the connection must be closed.
                        socket.close();
                        return;
                    }
                    if (response != null) {
                        // Send response to connected client.
                        if (!Packet.send(os, response, state.getCompression())) {
                            throw new IOException("Couldn't send packet!");
                        }
                    }
                }

            }
        } catch (IOException e) {
            LogUtils.getLogger().error("I/O error", e);
        } finally {
            try {
                close();
            } catch (IOException e) {
                LogUtils.getLogger().fatal("Cannot close", e);
            }
        }
    }

    @Override
    public void close() throws IOException {
        LogUtils.debug("Closing: " + this);
        // Raises SocketException(extends IOException) in the worker thread.
        socket.close();
        // Releases the authenticated status in the case the client abruptly disconnected.
        state.close(server.getStore());
        server.removeClient(this);
        // Note: we cannot join with the worker thread here, as this method may be called from inside it.
    }

    @Override
    public String toString() {
        return socket.getInetAddress().getHostName() + ":" + socket.getPort();
    }
}
