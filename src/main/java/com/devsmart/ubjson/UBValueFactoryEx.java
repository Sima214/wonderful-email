package com.devsmart.ubjson;

public class UBValueFactoryEx {

    public static UBInt16 createInt16(short v) {
        return new UBInt16(v);
    }

    public static UBInt64 createInt64(long v) {
        return new UBInt64(v);
    }
}
