# Wonderful Email

## Starting Server

### Gradle

`gradle run --args="server"`

### Distribution

Execute the appropriate file in bin.

## Starting Client

### Gradle

`gradle run --args="client"`

### Distribution

Execute the appropriate file in bin.

## Design notes

1. One of the most initial design goals was to have as much server-client common code as possible.
1. Email objects are (mostly) immutable once sent, and server-side email drafts are not supported.
1. No push notifications, clients will have to manually refresh every time.
1. You can send an email to only one other user at a time and not to yourself.
1. Forced TLS encryption.
1. The payload is encoded as a ubjson object, or nothing if specified as a 0-length.
1. The files created by the server are very fragile.
1. synchronize on everything that gets shared between threads. No time for proper locking.
1. If the client has sent a request, then no new requests must be sent until the response is received. This is achieved by I/O blocking the UI thread.
1. Any form of documentation may NOT be repeated, so pay close attention.
1. The server is designed to be especially resilient against malformed input. Sadly this came at the cost of reduced code quality.
1. There are default local keys available, so you can test it without worrying about creating and passing certificates(<-only works with distributions).
1. You shouldn't have more then one instance running in the same directory.

## Protocol details

All field are encoded in bigEndian and strings are utf-8.
Because of markdown limitations, if there is an empty field in the tables, that byte is part of the previous field.

### Connection overview

This Finite Automation overviews the lifecycle of a client-server connection in respect to the `payloadType`:

![See trapped_diag in doc](doc/trapped_diag.png)

Note that the action of the client closing the tcp connection has been omitted.

### Header Specifications

Each packet carries a 12-byte header:

|Offset|    0-7    |    8-15    |    16-23    |    23-31    |
|-----:|-----------|------------|-------------|-------------|
|  0x00|    'W'    |    'E'     |   version   |             |
|  0x04|   flags   |            | payloadType |             |
|  0x08|   length  |            |             |             |

- version: An array of two bytes. Currently both must be zero.
- flags: It's a bit-field. More specifically for each bit:
  - [0:1] 00: No compression, 01: ZLib compression, 10: Invalid, 11: Invalid
  - [2:31]: Reserved. Must be kept at zero.
- payloadType: The resulting CRC-16/KERMIT of the payload string.

Valid payload strings are:

1. handshake
2. register
3. logIn
4. listUsers
5. showEmails
6. readEmail
7. newEmail
8. deleteEmail
9. logOut

### Payload Specifications

#### handshake

Client and server agree on the protocol details.

Client sends its capabilities in the following format:

- `compression_types`(array): Array of integers(int16), which are the index ids of the supported compression types.

Server can respond with:

- `accepted`(boolean): True if the handshake is accepted.
- `message`(string): Optional message to display to the user.
- `compression_type`(integer): The accepted value from `compression_types`. May not exist if `accepted` is false.

Note that the server response is required only if the server accepts the client. The server can also completely ignore the client and close the tcp connection.

#### register

Client registers a new user with the server.

Client needs to send this information:

- `username`(string): Name of the user to register.
- `password`(string): Byte-string of the password.

Server can respond with:

- `accepted`(boolean): True if the registration is accepted and the user got authenticated.
- `message`(string): Displayed to the user.

#### logIn

A client authenticates as a user.

Client needs to send this information:

- `username`(string): Name of the user trying to log in.
- `password`(string): Byte-string of the password.

Server can respond with:

- `accepted`(boolean): True if the login is correct and the user got authenticated.
- `message`(string): Displayed to the user.

#### listUsers

An authenticated client requests a listing of all registered users.

Client sends a request (0 length payload).

Server responds with these fields:

- `user_list`(array): An array of _user_ objects(explained bellow)
  - `username`(string): Username.
  - `connected`(boolean): True if the user is currently online.

#### showEmails

An authenticated client requests a listing of their inbox.

Client sends a request (0 length payload).

Servers responds with the fields:

- `inbox`(array): An array of _inbox_ objects(explained bellow)
  - `display_name`(string): Name of the inbox
  - `listing`(array): An array of partial _email_ objects with only the bellow fields:
    - `uuid`(int64)
    - `received_from`(string)
    - `isRead`(boolean): True if this email has been viewed before.
    - `timestamp`(int64)
    - `subject`(string)

#### readEmail

An authenticated client requests an email object which exists in their inbox.

Client sends a request with these fields:

- `uuid`(int64): An email's UUID.

Server responds with an _email_ object, which contains the following fields:

- `uuid`(int64): UUID of the current email object.
- `sent_to`(string): Username of the user who received this email.
- `received_from`(string): Username of the user who sent this email.
- `timestamp`(int64): Unix millisecond timestamp.
- `reply_uuid`(int64): An **optional** UUID which refers to another email object.
- `subject`(string): Email component.
- `body`(string): Email component. Can be either clean text or html. This is auto detected by the clients.
- `attachments`(map): string->attachment mapping. The string is the filename and attachment . The attachment objects are as follows:
  - `mime_type`(string): https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
  - `data`(string): File contents encoded in base64.

#### newEmail

An authenticated client sends an email object, but:

1. The `received_from` field should not be present, as it will get ignored.

Client sends a request with an _email_ object(explained above).

Server responds with the fields:

- `sent`(boolean): True if the email was valid and could be sent.
- `message`(string): **Optional** message to display to the user.
- `uuid`(int64): The UUID of the sent email. If `sent` is false, then this field may not exist or be null.

#### deleteEmail

An authenticated client requests the deletion of an email object which exists in their inbox.

Client sends a request with these fields:

- `uuid`(int64): An email's UUID.

Server responds with these fields:

- `deleted`(boolean): True if an email with that uuid existed and the authenticated user had access to it, so it could be deleted.

#### logOut

A client de-authenticates itself.

Client sends a logout request (0 length payload).
The server acknowledges with a response of the same type.

## Server Implementation

Server creates a ServerClient for each incoming connection, which creates a ConnectionState.
For each Request received, a Response is created based on the ConnectionState and the data provided by the IServerStore, which is unique for the server and synchronizes access to the database.

## Client Implementation

Swing UI. Mostly dialogs. Communications are separated inside ClientProtocol, which is just a glue layer to the \*Packet\* classes.